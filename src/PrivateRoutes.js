import React from "react";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoutes = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (!props.match.params.agentId) {
        // not logged in so redirect to login page with the return url
        return (
          <Redirect
            to={{ pathname: "/agent", state: { from: props.location } }}
          />
        );
      }
      // authorised so return component
      return <Component {...props} />;
    }}
  />
);
