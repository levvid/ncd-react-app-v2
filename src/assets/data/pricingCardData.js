export const dentalPricingData = [
  {
    title: "1500 Plan",
    headerBgColor: "#D8ECF7",
    headerTextColor: "#303030",
    value: 1,
    cardBody: (
      <div>
        <h3>$1,500</h3>
        <h4>Annual Max</h4>
      </div>
    ),
    cardDesc: (
      <ul>
        <li>$1,500 annual max per person.</li>
        <li>
          $50 annual deductible for basic and major services (per person).
        </li>
        <li>$150 Max (per family).</li>
        <li>No deductible for preventative services.</li>
      </ul>
    ),
  },
  {
    title: "3000 Plan",
    headerBgColor: "#50C0E8",
    headerTextColor: "#303030",
    value: 2,
    cardBody: (
      <div>
        <h3>$3,000</h3>
        <h4>Annual Max</h4>
      </div>
    ),
    cardDesc: (
      <ul>
        <li>$3,000 annual max per person.</li>
        <li>
          $50 annual deductible for basic and major services (per person).
        </li>
        <li>$150 Max (per family).</li>
        <li>No deductible for preventative services.</li>
      </ul>
    ),
  },
  {
    title: "*3000 Plus Plan",
    headerBgColor: "#02477b",
    headerTextColor: "white",
    value: 3,
    cardBody: (
      <div>
        <h3>*$5,000</h3>
        <h4>Total Annual Max</h4>
      </div>
    ),
    cardDesc: (
      <ul>
        <li>
          <b>
            *$2,000 Plan buy up option for total $5,000 Plan benefit,
            self-funded by administrator.
          </b>
        </li>
        <li>
          $50 annual deductible for basic and major services (per person).
        </li>
        <li>$150 Max (per family).</li>
        <li>No deductible for preventative services.</li>
      </ul>
    ),
  },
];

export const visionPricingData = [
  {
    title: "Vision Plan",
    headerBgColor: "#D8ECF7",
    headerTextColor: "#303030",
    value: 1,
    cardBody: (
      <div>
        <h3>Vision Plan</h3>
        <h4>Annual Max</h4>
      </div>
    ),
    cardDesc: (
      <ul>
        <li>***Exam & glasses or contact lenses in lieu of glasses</li>
        <li>Guaranteed acceptance (no age limit restrictions)</li>
        <li>
          No waiting period (on your active date, you have access to the full
          range of vision benefits)
        </li>
      </ul>
    ),
  },
];
