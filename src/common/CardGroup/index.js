import {lazy, useState, useEffect} from 'react';
import {Form, Select, Row, Col} from 'antd';
import * as S from './styles';
import { NcdRadioButton } from '../../components/Banner/styles';

const Expand = lazy(() => import('react-expand-animated'));

const CardGroup = (props) => {
    let {
        cardsContent, 
        onChange, 
        defaultState, 
        containerStyles, 
        cardStyles,
        xl_default,
        buttonText,
        errorMessage,
        valueCallback,
        validationError,
        reset
    } = props;
    
    const [radioState, setRadioState] = useState(defaultState);
    
    useEffect(() => {
        setRadioState(defaultState);
    }, [reset]);

    const safelyHandleValueCallback = (value) => {
        try {
            valueCallback(value);
        } catch (e) {
            return;
        }
    };

    const handleClick = (e, value) => {
        setRadioState(value);
        safelyHandleValueCallback(value);
    };
    
    // initializing props
    let key = 0;
    defaultState = defaultState || null;
    buttonText = buttonText || 'Select';
    xl_default = xl_default || 0;
    errorMessage = errorMessage || 'Please select'
    valueCallback = valueCallback || (() => null);
    validationError = validationError || false;

    let cardsCount = cardsContent.length; 
    if (cardsCount === 0) {
        return (null);
    }
    
    let xs = 24;
    let xl = 24 / cardsCount;
    xl = (xl_default !== 0) ? xl_default : xl;

    return (
        <div>
            <Expand open={validationError && radioState === null}>
                <S.ErrorLabel>
                    {errorMessage}
                </S.ErrorLabel>
            </Expand>
            <S.NcdCardContainer gutter={24} styles={containerStyles}>
            {cardsContent.map(element => (
                <Col xs={xs} xl={xl} key={key++}>
                    <S.NcdCard style={cardStyles}>
                        {element.body}
                        <NcdRadioButton 
                            type="default"
                            style={(radioState == element.value) ? {
                                backgroundColor: '#4ec0e8',
                                color: 'white'
                            } : {
                                backgroundColor: 'white'
                            }}
                            onClick={(e) => handleClick(e, element.value)
                        }>
                            {(radioState == element.value) ? 'Selected' : buttonText}
                        </NcdRadioButton>
                    </S.NcdCard>
                </Col>
            ))}
            </S.NcdCardContainer>
        </div>
    );
};

export default CardGroup;