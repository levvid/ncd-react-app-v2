import styled from 'styled-components';
import {Row, Card, Button} from 'antd';

export const NcdCard = styled(Card)`
  display: block;
  border-radius: 10px;
  box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;
  width: 100%;
  margin: 0px 10px;
  padding: 0px 0px;
  p {
    font-size: 11px;
    font-weight: 400;
    color: rgb(51, 51, 51);
    line-height: 30px;
    margin-top: 20px;
  }
  strong {
    font-weight: 600;
  }
  .ant-card-body {
    padding: 0px 0px 10px 0px;
    width: 100%;
    text-align: center;
  }
  
  .card-desc {
    width: 100%;
    margin: 20px 0px;
  }
@media only screen and (max-width: 1000px) {
    min-height: 100px;
    min-width: 50px;
    padding-bottom: 10px;
    margin-bottom: 10px;
  } 
`;
export const NcdCardContainer=styled(Row)`
  display: flex;
  padding: 0px;
  border-radius: 10px;
  background:#ffffff;
  opacity: 1;
  justify-content: left;
  position: relative;
  width: 100%;
`;

export const NcdRadioButton = styled(Button)`
    background-color: white;
    color: #53C3E9;
    border-radius: 10px;
    width: 90%;
    &:hover {
      color: red;
    }
`;

export const ErrorLabel = styled.div`
  color: #f5222d;
  font-weight: 600;
  text-align: left;
  padding: 10px;
  margin: 5px 0px 0px 5px;
`;