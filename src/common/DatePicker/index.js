import {useState, useEffect} from 'react';
import {Row, Col} from 'antd';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import moment from 'moment';
import * as S from './styled';

const DatePicker = (props) => {
    let {value, placeholder, onChange = () => null, maxDate, minDate, maxDateMessage, minDateMessage, disabled=false} = props;
    maxDate = maxDate || moment().format('01/01/2100');
    minDate = minDate || moment.format('01/01/1900');
    const [selected, setSelected] = useState(value);

    useEffect(() => {
        onChange(selected);
    }, [selected]);

    useEffect(() => {
        setSelected(value);
    }, [value]);

    return (
        <Row gutter={24} style={{width: 380}}>
            <Col xl={10}>
                <MonthSelect
                    selected={selected}
                    setSelected={setSelected}
                    minDate={minDate}
                    maxDate={maxDate}
                    disabled={disabled}
                />
            </Col>
            <Col xl={6}>
                <DaySelect 
                    selected={selected}
                    setSelected={setSelected}
                    minDate={minDate}
                    maxDate={maxDate}
                    disabled={disabled}
                />
            </Col>
            <Col xl={8}>
                <YearSelect 
                    selected={selected}
                    setSelected={setSelected}
                    minDate={minDate}
                    maxDate={maxDate}
                    disabled={disabled}
                />
            </Col>
        </Row>
    );
};

const MonthSelect = (props) => {
    const availableOptions = [
        {
            value: '01',
            label: 'January'
        },
        {
            value: '02',
            label: 'February'
        },
        {
            value: '03',
            label: 'March'
        },
        {
            value: '04',
            label: 'April'
        },
        {
            value: '05',
            label: 'May'
        },
        {
            value: '06',
            label: 'June'
        },
        {
            value: '07',
            label: 'July'
        },
        {
            value: '08',
            label: 'August'
        },
        {
            value: '09',
            label: 'September'
        },
        {
            value: '10',
            label: 'October'
        },
        {
            value: '11',
            label: 'November'
        },
        {
            value: '12',
            label: 'December'
        },
    ];

    const {selected, setSelected, maxDate, minDate, disabled} = props;
    const [displayOptions, setDisplayOptions] = useState(availableOptions);

    useEffect(() => {
        let isMaxYear = moment(selected, 'MM/DD/YYYY').year() === moment(maxDate, 'MM/DD/YYYY').year();
        let isMinYear = moment(selected, 'MM/DD/YYYY').year() === moment(minDate, 'MM/DD/YYYY').year();
        let maxMonth = moment(maxDate, 'MM/DD/YYYY').month();
        let minMonth = moment(minDate, 'MM/DD/YYYY').month();
        let newDisplayOptions = availableOptions.slice(isMinYear ? minMonth : 0, isMaxYear ? maxMonth + 1 : availableOptions.length + 1);
        setDisplayOptions(newDisplayOptions);
    }, [selected]);

    const onChange = (e) => {
        let previous_date = moment(selected, 'MM/DD/YYYY');
        previous_date.set('month', parseInt(e.target.value) - 1);
        setSelected(previous_date.format('MM/DD/YYYY'));
    };

    return (
        <CustomSelect
            value={selected.split('/')[0]}
            onChange={onChange}
            options={displayOptions} 
            placeholder='Month'
            disabled={disabled}
        />
    );
};

const DaySelect = (props) => {
    const {selected, setSelected, maxDate, minDate, disabled} = props;
    const [rangeMin, setRangeMin] = useState(0);
    const [rangeMax, setRangeMax] = useState(31);

    useEffect(() => {
        let isMaxYear = moment(selected, 'MM/DD/YYYY').year() === moment(maxDate, 'MM/DD/YYYY').year();
        let isMinYear = moment(selected, 'MM/DD/YYYY').year() === moment(minDate, 'MM/DD/YYYY').year();
        let isMaxMonth = moment(selected, 'MM/DD/YYYY').month() === moment(maxDate, 'MM/DD/YYYY').month();
        let isMinMonth = moment(selected, 'MM/DD/YYYY').month() === moment(minDate, 'MM/DD/YYYY').month();
        let maxDateCondition = isMaxYear && isMaxMonth;
        let minDateCondition = isMinYear && isMinMonth;
        let month = moment(selected, 'MM/DD/YYYY').month();
        let year = moment(selected, 'MM/DD/YYYY').year();
        let monthDays = getMonthDays(month, year);
        
        if (maxDateCondition) {
            setRangeMax(moment(maxDate, 'MM/DD/YYYY').date());
        } else {
            setRangeMax(monthDays);
        }
        if (minDateCondition) {
            setRangeMin(moment(minDate, 'MM/DD/YYYY').date());
        } else {
            setRangeMin(1);
        }
    }, [selected]);

    const onChange = (e) => {
        let previous_date = moment(selected, 'MM/DD/YYYY');
        previous_date.set('date', parseInt(e.target.value));
        setSelected(previous_date.format('MM/DD/YYYY'));
    };

    return (
        <CustomSelectWithRange
            value={selected.split('/')[1]}
            onChange={onChange}
            placeholder='Day'
            rangeMax={rangeMax}
            rangeMin={rangeMin}
            disabled={disabled}
        />
    );
};

const YearSelect = (props) => {
    const {selected, setSelected, maxDate, minDate, disabled} = props;
    
    let rangeMin = moment(minDate, 'MM/DD/YYYY').year() || 1920;
    let rangeMax = moment(maxDate, 'MM/DD/YYYY').year() || moment().year();

    const onChange = (e) => {
        let previous_date = moment(selected, 'MM/DD/YYYY');
        previous_date.set('year', parseInt(e.target.value));
        setSelected(previous_date.format('MM/DD/YYYY'));
    };

    return (
        <CustomSelectWithRange
            value={selected.split('/')[2]}
            onChange={onChange}
            placeholder='Year'
            rangeMax={rangeMax}
            rangeMin={rangeMin}
            disabled={disabled}
        />
    );
};

const CustomSelect = (props) => {
    const {value, options, placeholder, onChange, disabled} = props;
    
    return (
        <>
            <S.CustomInputLabel id="demo-simple-select-filled-label">{placeholder}</S.CustomInputLabel>
            <S.CustomSelect
                value={value}
                onChange={onChange}
                disabled={disabled}
            >
                {options.map(option => (
                    <S.CustomMenuItem value={option.value}>
                        <>{option.label}</>
                    </S.CustomMenuItem>
                ))}
            </S.CustomSelect>
        </>
    );
};

const CustomSelectWithRange = (props) => {
    const {rangeMin, rangeMax} = props;
    const [options, setOptions] = useState([]);
    
    useEffect(() => {
        let tempOptions = [];
        for (let i=rangeMin; i<=rangeMax; i++) {
            tempOptions.push({
                value: (i < 10) ? `0${i}` : `${i}`,
                label: (i < 10) ? `0${i}` : `${i}`
            });
        }
        setOptions(tempOptions);
    }, [rangeMin, rangeMax]);

    return (
        <>
            <CustomSelect {...props} options={options}/>
        </>
    );
};

const getMonthDays = (month, year) => {
    year = parseInt(year);
    month = parseInt(month);
    if (month % 2 === 0 && month < 7) {
        return 31;
    } else if (month === 7) {
        return 31;
    } else if (month % 2 === 1 && month > 7) {
      return 31;
    } else if (month === 1) {
        if (year % 4 === 0) {
            return 29;
        } else {
            return 28;
        }
    } else {
        return 30;
    }
};

export default DatePicker;
