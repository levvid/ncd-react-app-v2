import styled from "styled-components";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

export const CustomSelect = styled(Select)`
    .MuiInput-input {
        color: rgba(0, 0, 0, 0.9) !important;
        font-weight: 400 !important;
    }
    .Mui-disabled {
        color: rgba(0, 0, 0, 0.60) !important;
    }
    width: 100%;
`;

export const CustomMenuItem = styled(MenuItem)`
    color: #000000 !important;
`;

export const CustomInputLabel = styled(InputLabel)`
    font-weight: 500 !important;
    color: rgba(0, 0, 0, 0.9) !important;
`;