import React, {useState} from 'react';
import {Select} from 'antd';
import MomentUtils from "@date-io/moment";
import moment from 'moment';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
  } from '@material-ui/pickers';

  export const NcdSelectWithIcon = ({value, onChange, dropdownItems, icon, placeholder, onCustomChange}) => {
    const [selectedState, setSelectedState] = useState(value || null);

    let customOnChange = (value) => {
        onChange(value);
        onCustomChange(value);
    };

    return (
        <Select
            showSearch
            optionLabelProp="label"
            value={value}
            placeholder={
                <React.Fragment>
                    {icon}
                    &nbsp; {placeholder}
                </React.Fragment>
            }
            style={{textAlign: 'left'}}
            onChange={customOnChange}
            defaultActiveFirstOption={false}
            optionFilterProp="children"
            filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            filterSort={(optionA, optionB) =>
            optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            }
        >
            {dropdownItems.map(item => (
                <Select.Option
                    key={item.value}
                    value={item.value}
                    label={
                        <React.Fragment>
                            {icon}
                            &nbsp; {item.label}
                        </React.Fragment>
                    }
                    autocomplete='state'
                >
                    {item.label}
                </Select.Option>
            ))}
        </Select>
    );
};

export const NcdDatePicker = ({value, onChange, minDate, maxDate, style={}, isDOB = 0, maxDateMessage, minDateMessage}) => {
    const [selectedDate, setSelectedDate] = useState(value || null);
    minDate = minDate || moment().subtract(120, 'years');
    maxDate = maxDate || moment().add(60, 'days');
    
    const triggerChange = (changedValue) => {
        onChange?.(changedValue);
    };

    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <div className='ant-input-affix-wrapper' style={style}>
                <KeyboardDatePicker
                    openTo={isDOB? 'year' : 'date'}
                    views={["year", "month", "date"]}
                    value={selectedDate}
                    placeholder='MM/DD/YYYY'
                    onChange={(value) => {
                        let newDate = moment(value).format('MM/DD/YYYY');
                        setSelectedDate(newDate);
                        triggerChange(newDate);
                    }}
                    format='MM/DD/YYYY'
                    maxDate={maxDate}
                    minDate={minDate}
                    maxDateMessage={maxDateMessage ? maxDateMessage : 'Date should not be after maximal date'}
                    minDateMessage={minDateMessage ? minDateMessage : 'Date should not be before minimal date'}
                />
            </div>
            
        </MuiPickersUtilsProvider>
    );
};