import React, { lazy } from "react";
import { Row, Col, Result } from "antd";
const SvgIcon = lazy(() => import("../../common/SvgIcon"));
const Expand = lazy(() => import("react-expand-animated"));

const ProductNotFound = ({ product, text }) => {
  return (
    <Expand open={product === "404"}>
      <Row gutter={24} >
        <Col xl={16} style={{ marginLeft: "auto", marginRight: "auto" }}>  {/*GM*/}
          <Result
            
            icon={<SvgIcon src="pnf.svg" />}
            title={
              <div style={{ fontSize: 18 }}>
                You do not have license to sell in this state.<br/>Please contact 8442844944
              </div>
            }
          />
        </Col>
      </Row>
    </Expand>
  );
};

export default ProductNotFound;
