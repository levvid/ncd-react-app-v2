import { lazy, useState, useEffect, useReducer, useCallback } from "react";
import { useHistory } from "react-router-dom";
import { Row, Col, Button, Divider, Popover, message } from "antd";
import queryString from "query-string";
import configData from "../../../config";
import axios from "axios";
import * as S from "../styles";
import { useStore } from "../../../Store.js";
import parse from "html-react-parser";
import VisionModal from './visionModal.js';
import * as M from '../forms/methods';

// mapping for converting dental benefits to vision benefits
const dentalToVision = {
  51: 51,
  41: 450,
  677: 21,
  21:  21,
};

const SvgIcon = lazy(() => import("../../../common/SvgIcon"));

const CartCard = ({setBeginApplication, productFields}) => {
  const { 
    cartOverview, 
    searchFields, 
    planForm, 
    apiResponse,
    setPlanForm,
    setSearchFields,
    setCartOverview,
  } = useStore((state) => ({
    cartOverview: state.cartOverview,
    searchFields: state.searchFields,
    planForm: state.planForm,
    apiResponse: state.apiResponse,
    setPlanForm: state.setPlanForm,
    setSearchFields: state.setSearchFields,
    setCartOverview: state.setCartOverview,
  }));

  const [isProcessing, setIsProcessing] = useState(false);
  const [cartProducts, setCartProducts] = useState([]);
  const [isVisionModalVisible, setIsVisionModalVisible] = useState(false);
  const [visionPrice, setVisionPrice] = useState(0);

  const history = useHistory();

  const initializeApplication = async (searchFields, planForm, cartOverview) => {
    let parsed = queryString.parse(window.location.search);
    let agentID = parsed.agentID;
    try {
      await productFields.validateFields();
      setIsProcessing(true);
      let search_state = searchFields.search_state || '';
      let search_benefits = searchFields.search_product_type || '';

      if (search_state.length === 0) {
        message.error('Please select state');
        setIsProcessing(false);
        return;
      }
      
      if (search_benefits.length === 0) {
        message.error('Please select product type');
        setIsProcessing(false);
        return;
      }

      let res;

      if (parsed.app_id !== undefined && parsed.app_id.length > 10) {
        res = await axios({
          method: "patch",
          url: `${configData.API_URL}/app`,
          data: {
            app_id: parsed.app_id,
            app_data: {
              searchFields: searchFields,
              planForm: planForm,
              cartOverview: cartOverview,
            },
            app_email: searchFields.banner_email,
            agentID: agentID,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
      } else {
        const planFormData = {...planForm};
        delete planForm.cov_bill_date;
        delete planForm.cov_eff_date;
        res = await axios({
          method: "post",
          url: `${configData.API_URL}/app`,
          data: {
            agentID: agentID,
            app_data: {
              searchFields: searchFields,
              planForm: planFormData,
              cartOverview: cartOverview,
            },
            app_email: searchFields.banner_email,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
      }

      let data = res.data;
      let app_id = data.app_id;
      message.success("Success. Redirecting...");
      history.push(`/apply?agentID=${agentID}&app_id=${app_id}&current=0`);
      setIsProcessing(false);
    } catch (e) {
      setIsProcessing(false);
      if (e.errorFields && e.errorFields.length){
        return;
      }
      message.error("Unable to process your request at the moment.");
    }
  };
  
  const handleAddingVision = async () => {
    // modify the search_product_type
    let newSearchFields = {
      ...searchFields,
      search_product_type: 'both'
    };
    let newPlanForm = {
      ...planForm,
      vision_benefits: dentalToVision[planForm.dental_benefits]
    };

    let tmp = searchFields;
    tmp['search_product_type'] = "both";
    setSearchFields(tmp);
    setPlanForm(newPlanForm);
    
    setIsVisionModalVisible(false);
    let newCartOverview = await M.calcPricing(newSearchFields, apiResponse, newPlanForm);
    setCartOverview(newCartOverview);
    initializeApplication(newSearchFields, newPlanForm, newCartOverview);
  };

  const handleNotAddingVision = async () => {
    setIsVisionModalVisible(false);
    initializeApplication(searchFields, planForm, cartOverview);
  };

  const handleSubmit = async () => {
    if (searchFields.search_product_type === 'dental' && apiResponse.states[searchFields.search_state].length > 3) {
      // we show the vision products modal
      let newSearchFields = {
        ...searchFields,
        search_product_type: 'both'
      };
      let newPlanForm = {
        ...planForm,
        vision_benefits: dentalToVision[planForm.dental_benefits]
      };
      let newCartOverview = await M.calcPricing(newSearchFields, apiResponse, newPlanForm);
      setVisionPrice(newCartOverview.visionCartValue);
      setIsVisionModalVisible(true);
    } else {
      // do not show any modal, process application directly
      initializeApplication(searchFields, planForm, cartOverview);
    }
  };

  return (
      <>
        {setBeginApplication(handleSubmit)}
        <S.CartCard>
          <Row gutter={24}>
            <Col xl={18} style={{ textAlign: "left" }}>
              <h3>
                Subtotal ({cartOverview.productCount}{" "}
                {cartOverview.productCount === 1 ? "plan" : "plans"})
              </h3>
            </Col>
            <Col xl={6} style={{ textAlign: "right" }}>
              <h3>
                <b>${cartOverview.cartValue}</b>/mo
              </h3>
            </Col>
          </Row>
          
          <S.CartButton
            type="primary"
            loading={isProcessing}
            onClick={handleSubmit}
          >
            Begin Application
          </S.CartButton>
          <Divider style={{ margin: "15px 0px 5px 0px" }} />
          <Row gutter={24}>
            <Col xl={24}>
            <>
              {cartOverview['displayProducts']?.map(productGroup => (
                <>
                  {productGroup.map(element => (
                    <>
                      <div
                      style={{
                        fontSize: 12,
                        minHeight: 15,
                        fontWeight: "bold",
                      }}
                      >
                        <>
                          {parse(element.label)}:&nbsp;
                          ${element.price}
                          <br />
                        </>
                      </div>
                    </>
                  ))}
                  <Divider style={{margin: 8}} />
                </>
              ))}
            </>
            </Col>
          </Row>
          <div style={{ textAlign: "left" }}>
            <Row gutter={24}>
              <Col xl={3}>
                <SvgIcon src="shield.svg" width="30" height="40" />
              </Col>
              <Col xl={21}>
                Apply with Confidence
                <br />
                All Personal Information is safe
              </Col>
            </Row>
          </div>
          <Divider style={{ margin: "5px 0px 10px 0px" }} />
          <SvgIcon src="norton-badge.png" width="400" height="60" />
        </S.CartCard>
        <VisionModal 
          isVisible={isVisionModalVisible}
          setIsVisible={setIsVisionModalVisible}
          handleAddingVision={handleAddingVision}
          handleNotAddingVision={handleNotAddingVision}
          visionPrice={visionPrice}
        />
      </>
    );
};

export default CartCard;
