import {lazy, useState, useEffect} from 'react';
import * as S from "../styles";
import {Badge} from 'antd';
import CardGroup from '../../../common/CardGroup';


const ReactCardFlip = lazy(() => import('react-card-flip'));

const PricingCardElement = ({title,headerBgColor,headerTextColor,children}) => {
    return (
        <S.NcdCard title={title} headStyle={{background:headerBgColor,color:headerTextColor}}>
            {children}
        </S.NcdCard>
    );
}

const FlippingElement = (props) => {
    const {frontContent, backContent, flip, setFlip, expand, setExpand} = props;
    const handleClick = (e) => {
        e.preventDefault();
        let temp = flip;
        setFlip(!temp);
        if (!temp) {
            setExpand(true);
        } else {
            setTimeout(() => {
                setExpand(false);
            }, 200);
        }
    };

    return (
        <ReactCardFlip 
            isFlipped={flip} 
            flipDirection="horizontal"
        >
            <div style={{padding: '20px 0px', height: (expand) ? 150:100, transition: 'height 1s ease'}}>
                {frontContent}
                <a onClick={handleClick}><b>View More</b></a>
            </div>
            <div>
                <br/>
                <S.NcdCardDesc>
                    <span style={{height: 300}}>
                        {backContent}
                    </span>
                </S.NcdCardDesc>
                <a onClick={handleClick}><b>View Less</b></a>
            </div>
        </ReactCardFlip>
    );
};

const PricingCards = ({valueCallback, reset, pricingData, benefitPricing = [0, 0, 0], xl_default, defaultState}) => {
    const [flip, setFlip] = useState(false);
    const [expand, setExpand] = useState(false);

    let cardsContent = [];
    for (let i=0; i<pricingData.length; i++) {
        let cardData = pricingData[i];
        let content = {
            body: (
                <PricingCardElement
                    title={cardData.title}
                    headerBgColor={cardData.headerBgColor}
                    headerTextColor={cardData.headerTextColor}
                >
                <Badge.Ribbon text={`$${benefitPricing[i]}/mo`} style={{visibility: (benefitPricing[i] === 0) ? 'hidden' : 'visible'}}>
                    <FlippingElement
                        frontContent={cardData.cardBody} 
                        backContent={cardData.cardDesc}
                        flip={flip}
                        setFlip={setFlip}
                        expand={expand}
                        setExpand={setExpand}
                    />
                </Badge.Ribbon>
                </PricingCardElement>
            ),
            value: cardData.value
        };
        cardsContent.push(content);
    }
    let key = 0;
    return (
        <div>
            <CardGroup
                cardsContent={cardsContent} 
                buttonText='Select Product' 
                defaultState={defaultState}
                valueCallback={valueCallback}
                reset={reset}
                xl_default={xl_default}
            />
        </div>
    );
};

export default PricingCards;