import { lazy, useState } from "react";
import { Divider, Button, Modal, Row, Col } from "antd";
import * as S from "../styles";
import { useStore } from "../../../Store";

export default function CheckOutModal({
  isVisible,
  setIsVisible,
  handleAddingVision,
  handleNotAddingVision,
  visionPrice
}) {
  const [isVisionPlanVisible, setIsVisionPlanVisible] = useState(false);
  
  const handleCancel = async () => {
    setIsVisible(false);
  };

  const handleVisionPlanSelect = async () => {
    setIsVisionPlanVisible(true);
  };
  
  const notNowButton = (
    <Button key="not_now" onClick={handleNotAddingVision}>
        Not Now
    </Button>
  );

  const viewPlanButton = (
    <Button key="view_plan" onClick={handleVisionPlanSelect}>
        View Plan
    </Button>
  );
 
  const addVisionButton = (
    <Button key="add_vision" type="primary" onClick={handleAddingVision}>
        Add Vision Coverage
    </Button>
  );

  return (
    <>
      <Modal
        title="Checkout"
        visible={isVisible}
        centered
        width={800}
        style={{ padding: '10px', top: 0}}
        onCancel={handleCancel}
        footer={(isVisionPlanVisible) ? [notNowButton, addVisionButton] : [notNowButton, viewPlanButton, addVisionButton]}
      >
        <Row style={{overflowX: 'hidden', paddingTop: 10}}>
            <p style={{color: 'black'}}>
                Would you like to add Five Star Vision Coverage for ${visionPrice}?
            </p>
            {(isVisionPlanVisible) ? (
                <>
                <p style={{color: 'black', fontSize: 15}}>
                    ***Exam and glasses or contact lenses in lieu of glasses <br/>
                    Guaranteed acceptance (no age limit restrictions) <br/>
                    No waiting period (on your active date, you have access to full range of vision benefits)
                </p>
            </>
            ) : (
                null
            )}
        </Row>
      </Modal>
    </>
  );
}
