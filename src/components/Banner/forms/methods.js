export const handleCart = async (searchFields, apiResponse, formData, setCartOverview) => {
    let cartDetails = await calcPricing(searchFields, apiResponse, formData);
    console.log("Cart dets: " + JSON.stringify(cartDetails));
    setCartOverview(cartDetails);
};

export const calcPricing = async (searchFields, apiResponse, formData) => {
    const search_state = searchFields['search_state'];
    const search_product_type = searchFields['search_product_type'];
    
    const dental_benefits = formData['dental_benefits'] || -1;
    const vision_benefits = formData['vision_benefits'] || -1;
    const dental_package_type = formData['dental_package_type'] || -1;

    const stateProducts = apiResponse['states'][search_state];
    const productInfo = apiResponse['products'];
    const productRules = apiResponse['rules'];

    let cartValue = 0;
    let productCount = 0;
    let cartProducts = []; // {productID: xx, benefitID: xx}
    
    if (['dental', 'both'].includes(search_product_type)) {
        if (dental_package_type === -1 || dental_benefits === -1) {
            return;
        }
        ++productCount;
        const baseProduct = Math.min(...stateProducts);
        const targetProduct = baseProduct + dental_package_type - 1;
    
        let basePrice = 0;
        let benefitLabel = '';

        productInfo[targetProduct]['benefits'].forEach(element => {
            if (element['product_benefit_code'] === dental_benefits) {
                basePrice = element['product_price'];
                benefitLabel = element['product_benefit_label'];
            }
        });

        cartProducts.push({
            productID: targetProduct,
            benefitID: dental_benefits,
            type: 'dental',
            label: productInfo[targetProduct].product_label + '-' + benefitLabel,
            labelBrief: 'Dental' + '-' + benefitLabel,
            price: basePrice
        });

        cartValue+=basePrice;
    }

    if (['vision', 'both'].includes(search_product_type)) {
        if (vision_benefits === -1) {
            return;
        }
        ++productCount;
        const targetProduct = Math.max(...stateProducts);

        let basePrice = 0;
        let temp = productInfo[targetProduct];
        let benefitLabel = '';

        if (productInfo[targetProduct].hasOwnProperty('isStateWise')) {
            temp = productInfo[targetProduct][search_state];
        }

        temp['benefits'].forEach(element => {
            if (element['product_benefit_code'] === vision_benefits) {
                basePrice = element['product_price'];
                benefitLabel = element['product_benefit_label'];
            }
        });
        
        cartProducts.push({
            productID: targetProduct,
            benefitID: vision_benefits,
            type: 'vision',
            label: temp.product_label + '-' + benefitLabel,
            labelBrief: 'Vision' + '-' + benefitLabel,
            price: basePrice
        });

        cartValue+=basePrice;
    }

    productRules.forEach(element => {
        let addRule = element['product_ids'].filter(value => cartProducts.includes(value)).length;
        let resProduct;
        let resProductBenefitId;
        element['product_ids'].forEach(product_id => {
            cartProducts.forEach(cart_product => {
                if (cart_product.productID == product_id) {
                    resProduct = product_id;
                    resProductBenefitId = cart_product.benefitID;
                    addRule = 1;
                }
            });
        });

        if (element.hasOwnProperty('states') && element['states'].length > 0 && !element['states'].includes(search_state)) {
            addRule = 0;
        }
        
        if (element.hasOwnProperty('exclude_benefit_ids') && element['exclude_benefit_ids'].includes(resProductBenefitId)) {
            addRule = 0;
        }

        if (addRule) {
            cartValue += element.price_attachment;
            cartProducts.push({
                productID: element['product_attachment'],
                type: 'attachments',
                label: element.label,
                labelBrief: element.label,
                price: element.price_attachment,
                resProduct: resProduct
            });
        }
    });
    
    let c = cartProducts || [];
    let temp = [];
    let count = 0;
    let dentalCartValue = 0;
    let visionCartValue = 0;
    for (let i=0; i<c.length; i++) {
      let item = c[i];
      if (item.type === 'dental' || item.type === 'vision') {
        if (item.type === 'dental') {
          dentalCartValue += item.price;
        } else if (item.type === 'vision') {
          visionCartValue += item.price;
        }
        temp[count] = [];
        temp[count].push(item);
        for (let j=0; j<c.length; j++) {
          let innerItem = c[j];
          let itemResProduct = innerItem.resProduct || -1;
          if (itemResProduct === item.productID) {
            if (item.type === 'dental') {
              dentalCartValue += innerItem.price;
            } else if (item.type === 'vision') {
              visionCartValue += innerItem.price;
            }
            temp[count].push(innerItem);
            break;
          }
        }
        count++;
      }
    }

    return {
        cartValue: cartValue,
        productCount: productCount,
        cartProducts: cartProducts,
        displayProducts: temp,
        dentalCartValue: dentalCartValue,
        visionCartValue: visionCartValue
    };
};