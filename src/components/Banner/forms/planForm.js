/* eslint-disable react/prop-types */
import React, { lazy, useEffect, useState } from "react";
import { Row, Col, Form, Divider, message } from "antd";
import Sticky from "react-sticky-el";
import queryString from "query-string";
import { DentalPlanSelect, VisionPlanSelect } from "../components/index.js";
import {
  dentalPricingData,
  visionPricingData,
} from "../../../assets/data/pricingCardData";
import CartCard from "../components/cart.js";
import ChatCard from "../../Chat";
import * as M from "./methods";
import * as S from "../styles";
import { CircledNumber } from "../../../common/Badge";
import { useStore } from "../../../Store";
let beginApplication = () => null;
// const { Step } = Steps;

const PricingCards = lazy(() => import("../components/pricingCards"));
const Expand = lazy(() => import("react-expand-animated"));
const ProductNotFound = lazy(() => import("../../../common/ProductNotFound"));

const PlanView = (props) => {
  const {
    searchFields,
    apiResponse,
    setCartOverview,
    planForm,
    setPlanForm,
    selectedState,
    setSearchFields
    // validateAgentData,
  } = useStore((state) => ({
    searchFields: state.searchFields,
    apiResponse: state.apiResponse,
    setCartOverview: state.setCartOverview,
    planForm: state.planForm,
    setPlanForm: state.setPlanForm,
    validateAgentData: state.validateAgentData,
    selectedState: state.selectedState,
    setSearchFields: state.setSearchFields
  }));

  // eslint-disable-next-line react/prop-types
  const { categ, forceUpdate } = props;
  const [error] = useState(false); //setError
  const [benefitPricing, setBenefitPricing] = useState([0, 0, 0]);
  const [visionBenefitPricing, setVisionBenefitPricing] = useState([0]);
  const [isProcessing, setIsProcessing] = useState(false);

  const handleSubmit = async () => {
    setIsProcessing(true);
    try {
      await beginApplication();
    } catch (e) {
      console.log(e);
    }
    setIsProcessing(false);
  };

  const handleBenefitPricing = async () => {
    try {
      let newBenefitPricing = [0, 0, 0];
      let newVisionBenefitPricing = [0];

      for (let i = 0; i < 3; i++) {
        let productBenefitPricing = await M.calcPricing(
          searchFields,
          apiResponse,
          {
            ...planForm,
            dental_package_type: i + 1,
          }
        );
        newBenefitPricing[i] = productBenefitPricing.dentalCartValue;
      }
      let visionBenefitCalc = await M.calcPricing(
        searchFields,
        apiResponse,
        planForm
      );
      newVisionBenefitPricing = [visionBenefitCalc.visionCartValue];
      setBenefitPricing(newBenefitPricing);
      setVisionBenefitPricing(newVisionBenefitPricing);
    } catch (e) {
      message.error("Unable to display comparative pricing");
    }
  };

  useEffect(async () => {
    let parsed = queryString.parse(window.location.search);
    let url_dental_benefits = parseInt(parsed.dental_benefits) || 51;  // GM
    let url_vision_benefits = parseInt(parsed.vision_benefits) || 51;  // GM
    let url_dental_package_type = parseInt(parsed.dental_package_type) || 3;

    let newPlanForm = {
      ...planForm,
      dental_benefits: url_dental_benefits,
      vision_benefits: url_vision_benefits,
      dental_package_type: url_dental_package_type,
    };

    setPlanForm(newPlanForm);
    const stateProducts = ['states'][selectedState];

    // if (stateProducts.length === 1) { // GM
    //   setSearchFields({search_state: selectedState, search_product_type: 'vision'});
    // } else if (stateProducts.length === 3) {
    //   setSearchFields({search_state: selectedState, search_product_type: 'dental'});
    // } else {
    //   setSearchFields({search_state: selectedState, search_product_type: 'both'});
    // }
    M.handleCart(searchFields, apiResponse, newPlanForm, setCartOverview);
  }, [categ, forceUpdate]);

  useEffect(async () => {
    try {
      if (
        ["dental", "vision", "both"].includes(searchFields.search_product_type)
      ) {
        M.handleCart(searchFields, apiResponse, planForm, setCartOverview);
      }
      handleBenefitPricing();
    } catch (e) {
      message.error("Unable to load products");
    }
  }, [planForm, forceUpdate]);

  const handleDentalSelect = async (value) => {
    let temp = planForm;
    temp["dental_benefits"] = value;
    console.log('Dental select: ' + value);
    setPlanForm(temp);
    M.handleCart(searchFields, apiResponse, planForm, setCartOverview);
    handleBenefitPricing();
  };

  const handleVisionSelect = async (value) => {
    let temp = planForm;
    temp["vision_benefits"] = value;
    console.log('Vision select: ' + value);
    setPlanForm(temp);
    M.handleCart(searchFields, apiResponse, planForm, setCartOverview);
    handleBenefitPricing();
  };

  const handlePackageSelect = async (value) => {
    let temp = planForm;
    temp["dental_package_type"] = value;
    console.log('Package select: ' + value);
    setPlanForm(temp);
    M.handleCart(searchFields, apiResponse, planForm, setCartOverview);
    handleBenefitPricing();
  };

  return (
    <Row gutter={24}>
      <Col xl={24}>
        <Expand
          open={["dental", "both"].includes(categ)}
          style={{ width: "100%" }}
          id
        >
          {/* planForm.dental_package_type */}
          <S.NcdDivider orientation="left">
            {<CircledNumber number={1} />}&nbsp;Select Dental Products
          </S.NcdDivider>
          <S.NcdStepperElement xl={24}>
            <PricingCards
              validationError={error}
              valueCallback={handlePackageSelect}
              reset={planForm.dental_package_type} // NEWESTGM
              pricingData={dentalPricingData}
              defaultState={planForm.dental_package_type} // NEWESTGM
              benefitPricing={benefitPricing}
            />
          </S.NcdStepperElement>
        </Expand>
      </Col>
      <Col xl={24}>
        <Expand open={["dental", "both"].includes(categ)}>
          <S.NcdDivider orientation="left">
            {<CircledNumber number={2} />}&nbsp;Dental Plan Benefits
          </S.NcdDivider>
          <S.NcdStepperElement xl={24}>
            <DentalPlanSelect
              validationError={error}
              valueCallback={handleDentalSelect}
              reset={planForm.dental_benefits} // GM
              defaultState={planForm.dental_benefits} // GM
            />
          </S.NcdStepperElement>
        </Expand>
      </Col>
      <Col xl={24}>
        <Expand open={["vision", "both"].includes(categ)}>
          <S.NcdDivider
            orientation="left"
            style={{ marginLeft: 0, paddingLeft: 0 }}
          >
            {<CircledNumber number={3 - (categ === "vision" ? 2 : 0)} />}
            &nbsp;Select Vision Products
          </S.NcdDivider>
          <S.NcdStepperElement xl={24}>
            <PricingCards
              validationError={error}
              valueCallback={handlePackageSelect}
              reset={categ}
              pricingData={visionPricingData}
              defaultState={1}
              benefitPricing={visionBenefitPricing}
              xl_default={8}
            />
          </S.NcdStepperElement>
        </Expand>
      </Col>
      <Col xl={24}>
        <Expand open={["vision", "both"].includes(categ)}>
          <S.NcdDivider
            orientation="left"
            style={{ marginLeft: 0, paddingLeft: 0 }}
          >
            {<CircledNumber number={4 - (categ === "vision" ? 2 : 0)} />}
            &nbsp;Vision Plan Benefits
          </S.NcdDivider>
          <S.NcdStepperElement xl={24}>
            <VisionPlanSelect
              validationError={error}
              valueCallback={handleVisionSelect}
              reset={planForm.vision_benefits} // GM
              defaultState={planForm.vision_benefits} // GM
            />
          </S.NcdStepperElement>
        </Expand>
      </Col>
      <Col xl={8} style={{ marginTop: 10, marginLeft: 10 }}>
        <S.CartButton
          type="primary"
          onClick={handleSubmit}
          loading={isProcessing}
        >
          Begin Application
        </S.CartButton>
      </Col>
    </Row>
  );
};

const PlanForm = (props) => {
  const { categ, forceUpdate, productFormFields } = props;
  const isMobile = window.innerWidth <= 768;

  const { 
    validateAgentData, 
    selectedState,
    searchFields,
  } = 
    useStore((state) => ({
    validateAgentData: state.validateAgentData,
    selectedState: state.selectedState,
    searchFields: state.searchFields,

  }));

  const checkValidAgent = ()=>{
    if (validateAgentData.isValidState &&
      !validateAgentData.isLicenseexpired &&
      !validateAgentData.isLicenSeSuspended && 
      validateAgentData.isValidIssueDate &&
      validateAgentData.isValidHealthLicence){
        return true;
      }
    else if (!validateAgentData.isValidState || 
      validateAgentData.isLicenSeSuspended || 
      validateAgentData.isLicenseexpired || 
      !validateAgentData.isValidIssueDate ||
      !validateAgentData.isValidHealthLicence){
        return false;
      }
      else {
        return false;
      }
  }

  const renderValidMessage = () => {
    if (!selectedState) {
      return;
    }
    if (
      validateAgentData.isValidState &&
      !validateAgentData.isLicenseexpired &&
      !validateAgentData.isLicenSeSuspended && 
      validateAgentData.isValidIssueDate &&
      validateAgentData.isValidHealthLicence
    ) {
      return <PlanView categ={categ} forceUpdate={forceUpdate} />;
    }
    if (!validateAgentData.isValidState || 
      validateAgentData.isLicenSeSuspended || 
      validateAgentData.isLicenseexpired || 
      !validateAgentData.isValidIssueDate ||
      !validateAgentData.isValidHealthLicence){
        return <ProductNotFound
          product="404"
          text={`Your License is valid only for ${validateAgentData.states}`}
        />
      }
  };

  return (
    <div style={{ width: "100%" }}>
      <Form size="large">
        <Row gutter={24}>
          <Col xl={16} xs={24}>
            {selectedState && renderValidMessage()}
          </Col>
          <Col xl={8} xs={24}>
            {checkValidAgent() && (
              <>
                <Divider orientation="left">Your Cart</Divider>
                {!isMobile ? (
                  <Sticky>
                    <div>
                      <CartCard
                        setBeginApplication={(f) => (beginApplication = f)} productFields={productFormFields} />
                      {/* <ChatCard /> */}
                    </div>
                  </Sticky>
                ) : (
                  <div>
                    <CartCard setBeginApplication={(f) => (beginApplication = f)} productFields={productFormFields} />
                    {/* <ChatCard /> */}
                  </div>
                )}
              </>
            )}
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default PlanForm;
