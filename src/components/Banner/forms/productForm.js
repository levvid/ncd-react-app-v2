import React, { lazy, useState, useEffect } from "react";
import { Row, Col, Radio, Form, Divider, Spin, message, Result } from "antd";
import queryString from "query-string";
import { withRouter } from "react-router";
import * as S from "../styles";
import axios from "axios";
import emailValidator from "email-validator";
import { NcdSelectWithIcon } from "../../../common/Input";
import ChatCard from "../../Chat";
import statesList from "../../../assets/data/statesList";
import { useStore } from "../../../Store";
import configData from "../../../config";

const Expand = lazy(() => import("react-expand-animated"));
const SvgIcon = lazy(() => import("../../../common/SvgIcon"));
const PlanForm = lazy(() => import("./planForm"));

const ProductForm = (props) => {
  const apiResponse = useStore((state) => state.apiResponse);
  const {
    searchFields,
    setSearchFields,
    setValidateAgentData,
    validateAgentData,
    agentLicenses,
    setSelectedState,
    selectedState,
    agentDetails,
    planForm, // GM
    setPlanForm, // GM
  } = useStore((state) => ({
    searchFields: state.searchFields,
    setSearchFields: state.setSearchFields,
    setValidateAgentData: state.setValidateAgentData,
    validateAgentData: state.validateAgentData,
    agentLicenses: state.agentLicenses,
    setSelectedState: state.setSelectedState,
    selectedState: state.selectedState,
    agentDetails: state.agentDetails,
    planForm: state.planForm, // GM
    setPlanForm: state.setPlanForm, // GM
  }));

  const [formData] = Form.useForm();
  const [formLayout] = useState("vertical"); //setFormLayout
  const [expand, setExpand] = useState(true);
  const [product, setProduct] = useState("both");
  const [isLoading, setIsLoading] = useState(false);
  const [forceUpdate, setForceUpdate] = useState(0);

  useEffect(async () => {
    let parsed = queryString.parse(window.location.search);
    const agentID = parsed.agentID;
    if (!agentID){
      props.history.push('/agent');
      return;
    }
    let search_state = parsed.state;
    let search_product_type = parsed.type;
    if (search_state !== undefined) {
      formData.setFieldsValue({ search_state: selectedState || search_state });
      if (search_product_type === undefined) {
        monitorStateChange(search_state || selectedState);
      } else {
        formData.setFieldsValue({ search_product_type: search_product_type });
        formData.submit();
      }
    }

    if (searchFields.search_product_type && searchFields.search_product_type != product && searchFields.search_product_type === "both") { // GM
      setProduct(searchFields.search_product_type);
      formData.setFieldsValue({search_product_type: 'both'}); // GM
    }
    
  }, [searchFields.search_product_type]); // GM

  useEffect(async () => {
    try {
      let parsed = queryString.parse(window.location.search);
      if (parsed.state !== undefined) {
          return;
      }
      let r = await axios({
          method: 'GET',
          url: `${configData.API_URL}/geo`,
      });
      if (apiResponse['states'].hasOwnProperty(selectedState || r.data.region_code)) {
          const stateProducts = apiResponse['states'][selectedState || r.data.region_code];
          formData.setFieldsValue({search_state: selectedState || r.data.region_code});
          setSelectedState(selectedState || r.data.region_code);
          if (stateProducts.length === 1) {
            formData.setFieldsValue({search_product_type: 'vision'});
          } else if (stateProducts.length === 3) {
            formData.setFieldsValue({search_product_type: 'dental'});
          } else {
            formData.setFieldsValue({search_product_type: 'both'});
          }
          // formData.submit();
      }
    } catch (e) {
      return;
    }
  }, []); // GM

  const onFinish = async (values) => {
    setIsLoading(true);
    let flag = 0;
    try {
      // debugger;
      // eslint-disable-next-line no-prototype-builtins
      if (apiResponse["states"].hasOwnProperty(values.search_state)) {
        const stateProducts = apiResponse["states"][values.search_state];
        if (values.search_product_type === "dental") {
          if (stateProducts.length >= 3) {
            flag = 1;
          }
        }
        if (values.search_product_type === "vision") {
          if (stateProducts.length === 1 || stateProducts.length === 4) {
            flag = 1;
          }
        }
        if (values.search_product_type === "both") {
          if (stateProducts.length >= 4) {
            flag = 1;
          }
        }
        if (flag) {
          console.log("Setting search fields: " + JSON.stringify(values));
          setSearchFields(values);
          setProduct(values.search_product_type);
          setExpand(true);
        } else {
          setExpand(false);
          setProduct("404");
        }
      } else {
        setExpand(false);
        setSearchFields(values);
        setProduct("404");
      }
    } catch (e) {
      message.error("Unable to load products");
    }
    setForceUpdate(1 - forceUpdate);
    setIsLoading(false);
  };

  const monitorStateChange = async(value) => {
    const stateLicense = agentLicenses.find((data) => data.STATE === value);
    const obj = { ...validateAgentData };
    obj.isValidState = !!stateLicense;
    let search_product_type;
    setSelectedState(value);
    setValidateAgentData({ ...obj, ...stateLicense });

    // GM
    let temp = planForm;
    temp["dental_benefits"] = 51;
    temp['vision_benefits'] = 51;
    temp["dental_package_type"] = 3; // NEWESTGM
    setPlanForm(temp);
    // GM - end
    
    if (apiResponse["states"][value]?.length === 1) {
      formData.setFieldsValue({ search_product_type: "vision" });
      search_product_type = "vision";
    }
    if (apiResponse["states"][value]?.length >= 3) {
      formData.setFieldsValue({ search_product_type: "both" }); // GM
      search_product_type = "both"; // GM
    }
    if (
      !(value in apiResponse["states"]) ||
      apiResponse["states"][value]?.length === 0
    ) {
      formData.setFieldsValue({ search_product_type: "404" });
      search_product_type = "404";
    }
    // formData.submit();
    onFinish({search_state: value, search_product_type});
    window.scrollTo(0, 0);
  };

  const handleEmailBlur = async (e) => {
    if (emailValidator.validate(e.target.value)) {
      setSearchFields({
        ...searchFields,
        banner_email: e.target.value,
      });
      //   let r = await axios({
      //     method: "POST",
      //     url: `${configData.API_URL}/email`,
      //     data: {
      //       email: e.target.value,
      //     },
      //   });
    } else {
      return;
    }
  };

  const checkValidAgent = () => {
    if (
      validateAgentData.isValidState &&
      !validateAgentData.isLicenseexpired &&
      !validateAgentData.isLicenSeSuspended &&
      validateAgentData.isValidIssueDate &&
      validateAgentData.isValidHealthLicence
    ) {
      return false;
    } else if (
      !validateAgentData.isValidState ||
      validateAgentData.isLicenSeSuspended ||
      validateAgentData.isLicenseexpired ||
      !validateAgentData.isValidIssueDate ||
      !validateAgentData.isValidHealthLicence
    ) {
      return true;
    } else {
      console.log('valid agent 2');
      return false;
    }
  };

  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      xl: {
        span: 24,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      xl: {
        span: 20,
      },
    },
  };

  return (
    <div style={{ width: "100%" }}>
      <Form
        {...formItemLayout}
        layout={formLayout}
        form={formData}
        initialValues={{}}
        onFinish={onFinish}
        onFinishFailed={() => {
          formData.setFieldsValue({ product: "unchecked" });
        }}
        style={{ width: "100%", marginBottom: 0 }}
        size="large"
      >
        <Row gutter={24} align="middle" justify="left">
          <Col xs={24} xl={6}>
            <Form.Item
              label="State:"
              name="search_state"
              rules={[
                {
                  required: true,
                  message: "Please select state",
                },
              ]}
            >
              <NcdSelectWithIcon
                icon={<SvgIcon src="map.png" height={25} />}
                dropdownItems={statesList}
                onCustomChange={monitorStateChange}
                placeholder="Select State"
              />
            </Form.Item>
          </Col>
          <Col xl={9} xs={24}>
            <Form.Item
              label="Email: "
              name="banner_email"
              rules={[
                {
                  required: true,
                  message: 'Please enter email.'
                },
                {
                  type: "email",
                  message: "Please enter a valid email.",
                },
              ]}
            >
              <S.NcdInput
                placeholder="Enter email"
                prefix={<SvgIcon src="envelope.png" height={30} />}
                onBlur={handleEmailBlur}
              />
            </Form.Item>
          </Col>
          <Col xl={9} xs={24}>
            <Form.Item
              label="Select products:"
              name="search_product_type"
              wrapperCol={{ sm: 24 }}
              rules={[
                {
                  required: true,
                  message: "Please select products",
                },
              ]}
            >
              {/* formData.validateFields(); formData.submit();*/}
              <Radio.Group
                className="ant-input-affix-wrapper"
                disabled={checkValidAgent()}
                style={{ padding: "7px 0px", fontWeight: 600 }}
                onChange={(e) => { 
                  onFinish({search_state: selectedState, search_product_type: e.target.value});
                }}
                value={product} 
                defaultValue={product}
              >
                <Radio value={"dental"}> {/*GM */}
                  <SvgIcon src="dental.png" height={30} />
                  Dental
                </Radio>
                <Radio value={"vision"}>  {/*GM */}
                  <SvgIcon src="vision.png" height={30} />
                  &nbsp;Vision
                </Radio>
                <Radio value={"both"}>Both</Radio>  {/*GM */}
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Divider style={{ marginTop: 0 }} />
      <br />
      <Expand open={isLoading}>
        <Spin size="large" />
      </Expand>
      <Expand open={expand}>
        <PlanForm categ={product} forceUpdate={forceUpdate} productFormFields={formData} /> {/* GM */}
      </Expand>

      <Expand open={product === "404"}>
        <Row gutter={24}>
          <Col xl={16}>
            <Result
              icon={<SvgIcon src="pnf.svg" />}
              title={
                <div style={{ fontSize: 18 }}>
                  You do not have license to sell in this state.<br/>Please contact 8442844944
                </div>
              }
            />
          </Col>
          <Col xl={8}>
            <ChatCard />
          </Col>
        </Row>
      </Expand>
    </div>
  );
};

export default withRouter(ProductForm);
