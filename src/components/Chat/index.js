import * as S from './styles';

const Chat = () => (
    <S.ChatCard>
        <h5>Have a question? Contact Us</h5>
        <h2>(888)-415-4801</h2>
        <h4><a href='mailto:dtc@nationalcaredental.com' style={{color: '#084566'}}>dtc@nationalcaredental.com</a></h4>
        <h5>Representatives are available: <br/>
        Mon-Fri 8am-6pm(CST)
        </h5>
    </S.ChatCard>
);

export default Chat;