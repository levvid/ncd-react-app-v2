import styled from 'styled-components';
import {Card} from 'antd';

export const ChatCard = styled(Card)`
  display: flex;
  justify-content: center;
  margin: 0px;
  text-align: center;
  z-index: 2000;
  padding: 0px;
  .ant-card-body {
    padding: 10px 0px;
  }
`;