import { useState, Fragment, lazy } from "react";
import { Row, Col } from "antd";

import * as S from "./styles";
import { useStore } from "../../Store";

const SvgIcon = lazy(() => import("../../common/SvgIcon"));

const Header = ({ t }) => {
  const isMobile = window.innerWidth <= 768;

  const { agentDetails } = useStore((state) => ({
    agentDetails: state.agentDetails,
  }));

  return (
    <S.Header>
      <S.Container>
        <Row type="flex" justify="space-between" gutter={24}>
          <Col xl={16} xs={10}>
            <S.LogoContainer aria-label="homepage">
              <Row
                type="flex"
                gutter={24}
                align="middle"
                style={{ height: "100%" }}
              >
                <Col xl={6}>
                  <SvgIcon src="logo.svg" height={100} />
                </Col>
                <Col xl={2} style={{ marginRight: 5 }} className="extra-logos">
                  <p style={{ fontSize: 8 }}>Dental underwritten by</p>
                </Col>
                <Col xl={5} className="extra-logos">
                  <SvgIcon
                    src="nationwide_logo.svg"
                    style={{ verticalAlign: "bottom" }}
                    height={50}
                  />
                </Col>
                <Col xl={2} style={{ marginRight: 5 }} className="extra-logos">
                  <p style={{ fontSize: 8 }}>Vision underwritten by</p>
                </Col>
                <Col xl={5} className="extra-logos">
                  <SvgIcon
                    src="vsp_logo.svg"
                    style={{ verticalAlign: "bottom" }}
                    height={50}
                  />
                </Col>
              </Row>
            </S.LogoContainer>
          </Col>
          <Col xl={8} xs={14}>
            {!!Object.keys(agentDetails).length && (
              <S.CallBox>
                <span className="light-span">Agent Name</span>
                <h3>
                  <b>{agentDetails.name}</b>
                </h3>
                <h5>{agentDetails.email}</h5>
              </S.CallBox>
            )}
          </Col>
          {/* <Col xl={4} xs={14}>
            <S.CallBox>
              <span className="light-span">Need help?</span>
              <h3>
                <b>{agentDetails.number || "(888)-415-4801"}</b>
              </h3>
              <h5>Mon-Fri 8am-6pm(CST)</h5>
            </S.CallBox>
          </Col> */}
        </Row>
      </S.Container>
    </S.Header>
  );
};

export default Header;
