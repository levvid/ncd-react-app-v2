import React, { useState } from "react";
import { Row, Col, Form, Input, Button, Card, message } from "antd";
import moment from "moment";
import { useStore } from "../../Store";
import axios from "axios";
import * as S from "../../components/Banner/styles";
import "./Agent.css";
// import { Link } from "react-router-dom";

const AgentLogin = ({ history }) => {
  const [loading, setLoading] = useState(false);
  const { 
    setAgentLicense, 
    setAgentData,
    setSelectedState,
    setValidateAgentData,
    setCartOverview
  } = useStore((state) => ({
    setAgentLicense: state.setAgentLicense,
    setAgentData: state.setAgentData,
    setSelectedState: state.setSelectedState,
    setValidateAgentData: state.setValidateAgentData,
    setCartOverview: state.setCartOverview
  }));

  const onFinish = async (values) => {
    
    try {
      setLoading(true);
      setAgentLicense([]);
      setAgentData({});
      setSelectedState("");
      setValidateAgentData({});
      setCartOverview({ cartValue: 0, productCount: 0 });
      const { data: agent } = await loginAgent(values.agentId);
      const { data } = await getAgentlicnese(values.agentId);
      const agentDetails = {
        name: agent.FIRSTNAME + " " + agent.LASTNAME,
        email: agent.EMAIL,
        number: agent.PHONE1,
        agentId: values.agentId,
      };
      setAgentData(agentDetails);
      const result = data.map((item) => {
        const todayDate = moment(new Date()).format("MM-DD-YYYY");
        const types = item.TYPES[0].split("|");
        item.isValidHealthLicence = !!types.indexOf("Health") !== -1;
        const skipCheck = !!(
          ["FL", "ME", "MI", "NC", "VA", "KS"].indexOf(item.STATE) !== -1
        );
        if (skipCheck) {
          item.isLicenSeSuspended = false;
          item.isLicenseexpired = false;
        } else {
          if (item.SUSPENDED_DATE) {
            const suspDate = moment(item.SUSPENDED_DATE).format("MM-DD-YYYY");
            item.isLicenSeSuspended = new Date(suspDate) < new Date(todayDate);
          } else {
            item.isLicenSeSuspended = false;
          }
          if (item.EXPIRATION_DATE) {
            const exDate = moment(new Date(item.EXPIRATION_DATE)).format(
              "MM-DD-YYYY"
            );
            item.isLicenseexpired = new Date(exDate) < new Date(todayDate);
          } else {
            item.isLicenseexpired = false;
          }
        }
        if (item.ISSUED_DATE) {
          const issueDate = moment(new Date(item.ISSUED_DATE)).format(
            "MM-DD-YYYY"
          );
          item.isValidIssueDate = new Date(issueDate) <= new Date(todayDate);
        } else {
          item.isValidIssueDate = false;
        }
        return item;
      });
      const validState = result.find(item=> {
        return !item.isLicenseexpired &&
        !item.isLicenSeSuspended && 
        item.isValidIssueDate &&
        item.isValidHealthLicence && item.STATE === 'TX' || item.STATE === 'CA';
      });
      if (validState){
        setSelectedState(validState.STATE);
        validState.isValidState = true;
        setValidateAgentData({ ...validState });
      }
      setAgentLicense([...result]);
      history.push({pathname: '/',search: "?agentID=" + values.agentId});
      setLoading(false);
    } catch (err) {
      message.error("Please enter a valid agent id");
      setLoading(false);
    }
  };

  const loginAgent = async (agentId) => {
    return await axios.get(
      `https://api.1administration.com/v2/agents/${agentId}`,
      {
        headers: {
          Authorization: "Basic TkNEQVBJOldlbGNvbWUxMjMh",
        },
      }
    );
  };

  const getAgentlicnese = async (agentId) => {
    return await axios.get(
      `https://api.1administration.com/v2/agents/${agentId}/licenses`,
      {
        headers: {
          Authorization: "Basic TkNEQVBJOldlbGNvbWUxMjMh",
        },
      }
    );
  };

  return (
    <div className="wrapper">
      <div className="wrapper_container loginSec">
        <Row type="flex" justify="end" align="middle">
          <S.AgentLeftCol span="16" className="loginLeft">
            <h1>Welcome to Agent Portal </h1>
            <p>
              Lorem Ipsum has been the industry's standard dummy text ever since
              the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book. It has survived not
              only five centuries, but also the leap into electronic
              typesetting, remaining essentially unchanged.
            </p>
          </S.AgentLeftCol>
          <S.AgentRightCol span="8" className="logiRight">
            <Card>
              <h2>AGENT LOGIN</h2>
              <Form layout="vertical" name="nest-messages" onFinish={onFinish}>
                <Form.Item
                  name="agentId"
                  label="Agent Id"
                  rules={[
                    { required: true, message: "Please enter your Agent id" },
                  ]}
                >
                  <Input placeholder="Enter Agent Id" />
                </Form.Item>
                <Button
                  loading={loading}
                  type="primary"
                  htmlType="submit"
                  block
                >
                  Procced
                </Button>
              </Form>
              {/* <Link to="" className="forgotLink">Forgot your Login Details?</Link> */}
            </Card>
          </S.AgentRightCol>
        </Row>
      </div>
    </div>
  );
};

export default AgentLogin;
