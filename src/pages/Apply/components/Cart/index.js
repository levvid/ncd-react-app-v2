import { lazy, useState } from "react";
import { Divider, Button, Row, Col } from "antd";
import * as S from "./styles";
import QuickEdit from "../quickEdit";
import { useStore } from "../../../../Store.js";
import { ArrowRightOutlined } from "@ant-design/icons";
import ChangeTimeline from "../changeTimeline";

import AddDentalPreviousCoverageModal from "../addDentalPreviousCoverageModal";  // MLG
const SvgIcon = lazy(() => import("../../../../common/SvgIcon"));

// const formItemLayout = {
//   labelCol: {
//     xs: {
//       span: 24,
//     },
//     sm: {
//       span: 8,
//     },
//     xl: {
//       span: 24,
//     },
//   },
//   wrapperCol: {
//     xs: {
//       span: 24,
//     },
//     sm: {
//       span: 16,
//     },
//     xl: {
//       span: 12,
//     },
//   },
// };

export const CartCard = (props) => {
  const { nextFunction, loading, nextText, sendApplication, emailLoading, textLoading } = props;

  const { cartOverview, planForm } = useStore((state) => ({
    cartOverview: state.cartOverview,
    planForm: state.planForm,
  }));

  const [isChangeModalVisible, setIsChangeModalVisible] = useState(false);
  const [isQuickEditModalVisible, setIsQuickEditModalVisible] = useState(false);
  const [isAddDentalPreviousCoverageModal, setIsAddDentalPreviousCoverageModal] = useState(false); // MLG

  return (
    <>
      <S.CartCard>
        <h3>
          <b>Application Summary</b>
        </h3>
        <div style={{ marginTop: 10, fontSize: 12 }}>
          <Row gutter={24} align="bottom">
            <Col xl={18}>
              {cartOverview?.cartProducts?.map((element, index) => (
                <div key={index} style={{ width: "100%" }}>
                  {element.type === "vision" ? (
                    <>
                      <SvgIcon src="vision.png" height={20} />
                      &nbsp;&nbsp;
                      <b>{element.labelBrief}</b>
                    </>
                  ) : null}
                  {element.type === "dental" ? (
                    <>
                      <SvgIcon src="dental.png" height={20} />
                      &nbsp;&nbsp;
                      <b>{element.labelBrief}</b>
                    </>
                  ) : null}
                </div>
              ))}
            </Col>
            <Col xl={6}>
              <Button
                size="small"
                style={{ fontSize: 10 }}
                onClick={() => {
                  setIsQuickEditModalVisible(true);
                }}
              >
                Quick edit
              </Button>
            </Col>
          </Row>
        </div>
        <Divider />
        <div>
          <Row gutter={24} style={{ fontSize: 14 }}>
            <Col xl={12} style={{ padding: "0 0 0 10 !important", margin: 0 }}>
              <b>Effective Date</b>
              <br />
              {planForm.cov_eff_date || (
                <span style={{ fontSize: 11 }}>MM/DD/YYYY</span>
              )}
              &nbsp;
              <Button
                onClick={() => {
                  setIsChangeModalVisible(true);
                }}
                size="small"
              >
                Change
              </Button>
            </Col>
            <Col xl={12} style={{ padding: "0 10 0 0 !important" }}>
              <b>Billing Date</b>
              <br />
              {planForm.cov_bill_date || (
                <span style={{ fontSize: 11 }}>MM/DD/YYYY</span>
              )}
              &nbsp;
              <Button
                onClick={() => {
                  setIsChangeModalVisible(true);
                }}
                size="small"
              >
                Change
              </Button>
            </Col>
          </Row>
        </div>
        <Divider style={{ marginTop: 5 }} />
        <S.CartButton
          type="primary"
          size="large"
          onClick={nextFunction}
          loading={loading}
        >
          {nextText}
          <ArrowRightOutlined style={{ color: "white" }} />
        </S.CartButton>
        <Divider />
        <div style={{ marginTop: 10, fontSize: 12 }}>
        <Row gutter={24} align="bottom" justify="center">
          <Col xl="8" style={{'padding':'5px'}}>
          <h3>
            <b>Send Application</b>
          </h3>
          </Col>
          <Col xl="8">
          <S.CartButton
          type="primary"
          size="large"
          onClick={()=>sendApplication('email')}
          loading={emailLoading}
        >
          Email
        </S.CartButton>
          </Col>
          <Col xl="8">
          <S.CartButton
          type="primary"
          size="large"
          onClick={()=>sendApplication('text')}
          loading={textLoading}
        >
          Text
        </S.CartButton>
          </Col>
        </Row>
        </div>
      </S.CartCard>
      <ChangeTimeline
        isVisible={isChangeModalVisible}
        setIsVisible={setIsChangeModalVisible}
      />
      <QuickEdit
        isVisible={isQuickEditModalVisible}
        setIsVisible={setIsQuickEditModalVisible}
        setIsAddDentalPreviousCoverageModal={setIsAddDentalPreviousCoverageModal} 
      />
      {/*MLG */}
      <AddDentalPreviousCoverageModal 
        isVisible={isAddDentalPreviousCoverageModal}
        setIsVisible={setIsAddDentalPreviousCoverageModal}
      />
      {/*MLG */}
    </>
  );
};

export default CartCard;
