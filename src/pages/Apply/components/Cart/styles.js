import styled from "styled-components";
import { Card, Button, Divider } from "antd";

export const CartCard = styled(Card)`
  text-align: left;
  margin: 7px 0px 20px 0px;
  .ant-card-body {
    padding: 20px 10px;
  }
`;

export const CartButton = styled(Button)`
  width: 100%;
  margin: 0px 0px 0px 0px;
  background-color: #02477b;
`;

export const NcdDivider = styled(Divider)`
  margin: 5px 0px 10px 0px;
`;
