import {
    Button,
    Result,
    Typography,
} from "antd";
import { Link } from "react-router-dom";
import { CheckOutlined } from "@ant-design/icons";
import queryString from "query-string";
const { Paragraph, Text } = Typography;

let parsed = queryString.parse(window.location.search);
const agentID = parsed.agentID;

const SuccessPage = () => (
  <Result
    status="success"
    title="Application submitted successfully"
    extra={[
      <Link to={`/?agentID=${agentID}`}>
        <Button type="primary">Start a new application</Button>
      </Link>,
    ]}
  >
    <div className="desc">
      <Paragraph>
        <Text
          strong
          style={{
            fontSize: 16,
          }}
        >
          You will hear from us soon, in the meantime you can:
        </Text>
      </Paragraph>
      <Paragraph>
        <CheckOutlined className="site-result-demo-error-icon" /> Read more
        about our offerings <Link to="">here</Link>
      </Paragraph>
      <Paragraph>
        <CheckOutlined className="site-result-demo-error-icon" /> Send us an email at{" "} {/*GM*/}
        <a href="mailto:info@nationalcaredental.com">
          info@nationalcaredental.com
        </a>{" "}
        if you have any questions.
      </Paragraph>
    </div>
  </Result>
);

export default SuccessPage;
