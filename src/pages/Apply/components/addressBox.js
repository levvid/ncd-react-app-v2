import { useState } from "react";
import { Input, Form, Row, Col, Radio, AutoComplete, Select, message } from "antd";
import { useStore } from "../../../Store";
import configData from "../../../config";

// data
import statesList from "../../../assets/data/statesList";
import SmartyStreetsSDK from "smartystreets-javascript-sdk";

const SmartyStreetsCore = SmartyStreetsSDK.core;
const Lookup = SmartyStreetsSDK.usAutocompletePro.Lookup;

let key = configData.SMARTY_WEBSITE_KEY;
let hostname = "ncd-react-app.s3-website-us-west-2.amazonaws.com";
const credentials = new SmartyStreetsCore.SharedCredentials(key, hostname);

let clientBuilder = new SmartyStreetsCore.ClientBuilder(
  credentials
).withLicenses(["us-autocomplete-pro-cloud"]);

let client = clientBuilder.buildUsAutocompleteProClient();

const AddressForm = (props) => {
  let { form, field_prefix, title } = props;

  const {
    searchFields,
  } = useStore((state) => ({
    searchFields: state.searchFields,
  }));

  field_prefix = field_prefix || "";

  const [options, setOptions] = useState([]);
  const [address, setAddresses] = useState({});

  let fieldsInterface = {
    address1: `${field_prefix}address1`,
    address2: `${field_prefix}address2`,
    city: `${field_prefix}city`,
    state: `${field_prefix}state`,
    zip_code: `${field_prefix}zip_code`,
  };

  const onSelect = (val, option) => {
    let details = option.details;

    if (details.state !== searchFields.search_state) {
      message.info('You have selected a different state');
    }

    let newAddress = {};
    newAddress[fieldsInterface.address1] = details.streetLine;
    newAddress[fieldsInterface.address2] = details.secondary || "";
    newAddress[fieldsInterface.city] = details.city;
    newAddress[fieldsInterface.zip_code] = details.zipcode;
    newAddress[fieldsInterface.state] = details.state;

    form.setFieldsValue(newAddress);
  };

  const onSearch = async (searchText) => {
    try {
      if (!searchText) {
        return;
      }

      let lookup = new Lookup(searchText);

      let r = await client.send(lookup);
      let result = r.result;

      let suggestions = [];

      for (let i = 0; i < result.length; i++) {
        let streetLine = result[i].streetLine;
        let secondary = result[i].secondary;
        let city = result[i].city;
        let state = result[i].state;

        let value = `${streetLine}${secondary.length ? ", " + secondary : ""}${
          city.length ? ", " + city : ""
        }${state.length ? ", " + state : ""}`;

        suggestions.push({
          details: result[i],
          value: value,
        });
      }

      setOptions(suggestions);
    } catch (e) {
      setOptions([
        {
          details: {},
          value: searchText,
        },
      ]);
    }
  };

  return (
    <>
      <Col xl={24} xs={24}>
        <p style={{ color: "black", fontSize: "3.45vh" }}>
          <b>{title}</b>
        </p>
        <Row gutter={24}>
          <Col xl={8} xs={24}>
            <Form.Item
              name={field_prefix + "address1"}
              label="Street"
              rules={[
                {
                  type: "string",
                  required: true,
                  message: 'Please enter street'
                },
              ]}
            >
              <AutoComplete
                options={options}
                placeholder="Enter street"
                onSelect={(val, option) => onSelect(val, option)}
                onSearch={onSearch}
              />
            </Form.Item>
          </Col>
          <Col xl={8} xs={24}>
            <Form.Item
              name={field_prefix + "address2"}
              label="Apartment (Optional)"
              rules={[
                {
                  type: "string",
                },
              ]}
            >
              <Input placeholder="Enter Apartment Number" className='non-mandatory' />
            </Form.Item>
          </Col>
          <Col xl={8} xs={24}>
            <Form.Item
              name={field_prefix + "city"}
              label="City"
              rules={[
                {
                  required: true,
                  type: "string",
                  message: "Please input your City name",
                },
              ]}
            >
              <Input placeholder="Enter city" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24} justify="start">
          <Col xs={24} xl={8}>
            <Form.Item
              label="State:"
              name={field_prefix + "state"}
              rules={[
                {
                  required: true,
                  message: "Please select state",
                },
              ]}
            >
              <Select options={statesList} placeholder="Select State" />
            </Form.Item>
          </Col>
          <Col xs={24} xl={8}>
            <Form.Item
              name={field_prefix + "zip_code"}
              label="Postal Code"
              rules={[
                {
                  required: true,
                  message: "Please enter postal code"
                },
                {
                  pattern: new RegExp(/^[0-9]+$/),
                  message: "Not a valid Zip code",
                },
              ]}
            >
              <Input placeholder="Enter Zip code" type="number" />
            </Form.Item>
          </Col>
        </Row>
      </Col>
    </>
  );
};

const AddressBox = (props) => {
  let { form } = props;

  const {
    applicantForm,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
  }));
  
  let show_billing_form = ('bill_address_same' in applicantForm) ? applicantForm.bill_address_same : true;

  const [isMailAddressSame, setIsMailAddressSame] = useState(show_billing_form);

  return (
    <>
      <AddressForm form={form} field_prefix="" title="Home Address" />
      <Col xl={24}>
        <Form.Item
          labelCol={14}
          wrapperCol={10}
          name="bill_address_same"
          label="Is your billing address same from residential address?"
          initialValue={true}
        >
          <Radio.Group
            onChange={(e) => {
              setIsMailAddressSame(e.target.value);
            }}
          >
            <Radio value={true}>Yes</Radio>
            <Radio value={false}>No</Radio>
          </Radio.Group>
        </Form.Item>
      </Col>
      {!isMailAddressSame ? (
        <AddressForm
          form={form}
          field_prefix="billing_"
          title="Billing Address"
        />
      ) : (null)}
    </>
  );
};

export default AddressBox;
