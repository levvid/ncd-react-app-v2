import { useState, useEffect } from "react";
import {
  Button,
  Modal,
  Form,
  message,
  Typography,
  Select,
  Row,
  Col
} from "antd";
import NcdDatePicker from '../../../common/DatePicker';
import { CheckOutlined, EditOutlined } from "@ant-design/icons";
import { useStore } from "../../../Store.js";
import moment from "moment-timezone";
import axios from 'axios';
import configData from "../../../config";

const { Paragraph, Text } = Typography;

const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
      xl: {
        span: 24,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
      xl: {
        span: 18,
      },
    },
  };

const ChangeTimeline = (props) => {
    const { planForm, setPlanForm, appID } = useStore((state) => ({
      planForm: state.planForm,
      setPlanForm: state.setPlanForm,
      appID: state.appID
    }));

    const [form] = Form.useForm();
    const [formLayout] = useState("vertical");
  
    const { isVisible, setIsVisible, customFunction } = props;
    const [isLoading, setIsLoading] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);
    
    const handleEffDateChange = (value) => {
      let newEffDate = moment(value);
      let newBillDate = newEffDate.subtract(1, 'months').startOf('month').add(14, 'days');
      if (newBillDate.diff(moment(), 'days') < 0) {
        newBillDate = moment();
      }
      form.setFieldsValue({cov_bill_date: newBillDate.format('MM/DD/YYYY')});
      setMaxBillingDate(value);
      setPlanForm({
        ...planForm, 
        cov_eff_date: value,
        cov_bill_date: newBillDate.format('MM/DD/YYYY')
      });
    };

    let allowedEffDates = [];

    if (moment().tz('America/New_York').format("DD") < "25") {
      allowedEffDates.push(moment().tz('America/New_York').add(1, "M").startOf("month").format("MM/DD/YYYY"));
      allowedEffDates.push(moment().tz('America/New_York').add(2, "M").startOf("month").format("MM/DD/YYYY"));
    } else {
      allowedEffDates.push(moment().tz('America/New_York').add(2, "M").startOf("month").format("MM/DD/YYYY"));
      allowedEffDates.push(moment().tz('America/New_York').add(3, "M").startOf("month").format("MM/DD/YYYY"));
    }
    
    const minDate = moment();

    const [maxBillingDate, setMaxBillingDate] = useState(moment(planForm.cov_eff_date || allowedEffDates[0]));
    
    useEffect(() => {
      handleEffDateChange(allowedEffDates[0]);
    }, []);

    const handleCancel = () => {
      try {
        try {
          customFunction(0);
        } catch (e) {

        }
      } catch (e) {
        
      }
      setIsVisible(false);
    };
  
    const handleOk = async () => {
      let isValid =
        form.getFieldValue("cov_eff_date") >= form.getFieldValue("cov_bill_date");
      if (!isValid) {
        message.error("Billing date must be before or same as the effective date");
      } else {
        try {
          await onFinish();
          try {
            customFunction(1);
          } catch (e) {

          }
        } catch (e) {
          try {
            customFunction(0);
          } catch (e) {

          }
        }
        setIsVisible(false);
        message.success('Timeline changed');
      }
    };
  
    const onFinish = async () => {
      setIsLoading(true);
      
      let values = form.getFieldsValue();
      
      try {
        let new_plan_form = {
          ...planForm,
          ...values,
          cov_dates_changed: 1
        };
        setPlanForm(new_plan_form);
        let res = await axios({
          method: "patch",
          url: `${configData.API_URL}/app`,
          data: {
            app_data: {
              planForm: new_plan_form,
            },
            app_id: appID,
          },
          headers: {
            "Content-Type": "application/json",
          },
        });
        setIsLoading(false);
      } catch (e) {
        setIsLoading(false);
        throw e;
      } 

    };
    
    let key = 0;

    return (
      <>
        <Modal
          title="Change coverage timeline"
          visible={isVisible}
          onCancel={handleCancel}
          footer={[
            <Button key="cancel" onClick={handleCancel}>
              Cancel
            </Button>,
            <Button key="submit" type="primary" onClick={handleOk} loading={isLoading}>
              Continue
            </Button>,
          ]}
          style={{top: 30}}
        >
          <Form
            {...formItemLayout}
            form={form}
            formLayout={formLayout}
            onFinish={onFinish}
            initialValues={planForm}
            size='large'
          >
            <Form.Item 
              label="Coverage Effective Date" 
              name="cov_eff_date"
              initialValue={allowedEffDates[0]}
            >
              <Select
                onChange={handleEffDateChange}
                placeholder='MM/DD/YYYY'
              >
                {allowedEffDates.map(element => (
                  <Select.Option key={key++} value={element}>
                    {element}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Row gutter={24} type='flex' align='middle'>
              <Col xl={18}>
                <Form.Item 
                  label="First Billing Date" 
                  name="cov_bill_date"
                  initialValue={moment().format('MM/DD/YYYY')}
                >
                  <NcdDatePicker
                    minDate={minDate}
                    maxDate={maxBillingDate} 
                    disabled={isDisabled}
                  />
                </Form.Item>
              </Col>
              <Col xl={2}>
                <br/>
                <Button
                  onClick={() => setIsDisabled(!isDisabled)}
                >
                  <EditOutlined style={{fontSize: 30}} />
                </Button>
              </Col>
            </Row>
          </Form>
          <div className="desc">
            <Paragraph>
              <Text
                strong
                style={{
                  fontSize: 16,
                }}
              >
                Note that:
              </Text>
            </Paragraph>
            <Paragraph>
              <CheckOutlined className="site-result-demo-error-icon" /> Effective
              date must be up to 60 days from today.
            </Paragraph>
            <Paragraph>
              <CheckOutlined className="site-result-demo-error-icon" /> The
              billing date must be before or same as the effective date.
            </Paragraph>
          </div>
        </Modal>
      </>
    );
  };

  export default ChangeTimeline;