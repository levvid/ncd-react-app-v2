import { lazy, useState } from "react";
import { Divider, Button, Modal, Row, Col } from "antd";
import * as S from "../styles";
import { useStore } from "../../../Store";

export default function CheckOutModal({
  isVisible,
  setIsVisible,
  customFunction,
}) {
  const {
    applicantForm,
    paymentForm,
    appID,
    prevInsurance,
    planForm,
    cartOverview,
    paymentInfoForm,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
    appID: state.appID,
    paymentForm: state.paymentInfoForm,
    prevInsurance: state.prevInsuranceForm,
    planForm: state.planForm,
    cartOverview: state.cartOverview,
    paymentInfoForm: state.paymentInfoForm,
  }));

  let applicant_name =
    applicantForm.applicant_fname +
    (applicantForm.applicant_mname ? ` ${applicantForm.applicant_mname}` : "");
  applicant_name = applicant_name + " " + applicantForm.applicant_lname;

  const handleOk = async () => {
    customFunction(1);
    setIsVisible(false);
  };

  const handleCancel = async () => {
    customFunction(0);
    setIsVisible(false);
  };

  let dependent_children_key = 0;

  return (
    <>
      <Modal
        title="Checkout"
        visible={isVisible}
        centered
        width={1200}
        style={{ padding: '10px', top: 0}}
        onCancel={handleCancel}
        footer={[
          <Button key="cancel" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk}>
            Checkout
          </Button>,
        ]}
      >
        <Row style={{overflowY: 'scroll', height: '50vh', overflowX: 'hidden'}}>
          <Col xs={24} xl={24}>
            <>
              <S.tableHeadingDiv>
                <S.tableColHeading>Item(s)</S.tableColHeading>
                <S.tableColHeading>Amount($)</S.tableColHeading>
              </S.tableHeadingDiv>
              <div>
                {cartOverview?.displayProducts?.map((productGroup) => (
                  <>
                    {productGroup.map((element) => (
                      <S.tableRowDiv>
                        <S.fieldName>{element.labelBrief}</S.fieldName>
                        <S.fieldName fontWeight={"bold"}>
                          ${element.price}
                        </S.fieldName>
                      </S.tableRowDiv>
                    ))}
                    <Divider style={{ margin: 8 }} />
                  </>
                ))}
              </div>
              <Divider style={{ margin: "10px" }}></Divider>
              <S.tableFooterDiv>
                <S.tableFooterContent>
                  Estimated Initial Payment &nbsp;&nbsp;
                  <b>${cartOverview?.cartValue}</b>
                </S.tableFooterContent>
              </S.tableFooterDiv>
              <div>
                <S.tableRowDiv>
                  <S.fieldName fontWeight={"bold"}>
                    Ongoing Payment Type
                  </S.fieldName>
                  <S.fieldName>
                    {paymentInfoForm.payment_method_type == "ach"
                      ? "ACH"
                      : "CREDIT CARD"}
                  </S.fieldName>
                </S.tableRowDiv>
                <S.tableRowDiv>
                  <S.fieldName fontWeight={"bold"}>Applicant Name</S.fieldName>
                  <S.fieldName>{applicant_name}</S.fieldName>
                </S.tableRowDiv>
                <S.tableRowDiv>
                  <S.fieldName fontWeight={"bold"}>
                    Coverage Effective Date
                  </S.fieldName>
                  <S.fieldName>{planForm.cov_eff_date}</S.fieldName>
                </S.tableRowDiv>
                <S.tableRowDiv>
                  <S.fieldName fontWeight={"bold"}>
                    First Billing Date
                  </S.fieldName>
                  <S.fieldName>{planForm.cov_bill_date}</S.fieldName>
                </S.tableRowDiv>
              </div>
            </>
          </Col>
        </Row>
      </Modal>
    </>
  );
}
