import {useState} from 'react';
import { Form, Input, Button, Col, Row, Radio, Collapse } from "antd";
import moment from 'moment';
import NcdDatePicker from '../../../common/DatePicker';
import {useStore} from '../../../Store';
import * as S from "./styled";
import { DeleteOutlined } from "@ant-design/icons";
import React from 'react';

const {Panel} = Collapse;

const DepInfoForm = (props) => {
  const { key, name, fieldKey, restField, remove, removeEffects, title, minDOB, maxDOB, defaultGender, defaultDOB} = props;
  return (
    <S.DepBox>
      <h3>{title}</h3>
      <Row gutter={24}>
        <Col xs={24} xl={10}>
          <Form.Item
            {...restField}
            name={[name, "dependent_fname"]}
            fieldKey={[fieldKey, "dependent_fname"]}
            label="First Name"
            rules={[
              {
                type: 'string',
                required: true,
                message: 'Please enter First Name'
              },
              {
                pattern: new RegExp(/^[A-Za-z]+$/),
                message: 'Name cannot contain spaces or symbols'
              }
            ]}
          >
            <Input placeholder="FIRST NAME" />
          </Form.Item>
        </Col>
        <Col xs={24} xl={10}>
          <Form.Item
            {...restField}
            name={[name, "dependent_lname"]}
            fieldKey={[fieldKey, "dependent_lname"]}
            label="Last Name"
            rules={[
              {
                type: 'string',
                required: true,
                message: 'Please enter Last Name'
              },
              {
                pattern: new RegExp(/^[A-Za-z]+$/),
                message: 'Name cannot contain spaces or symbols'
              }
            ]}
          >
            <Input placeholder="LAST NAME" />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} xl={10}>
          <Form.Item
            {...restField}
            name={[name, "dependent_gender"]}
            fieldKey={[fieldKey, "dependent_gender"]}
            initialValue={defaultGender}
            label="Gender"
            rules={[
              {
                required: true,
                message: 'Please select gender'
              }
            ]}
          >
            <Radio.Group
              className="ant-input-affix-wrapper"
              style={{
                padding: "8px 5px",
                fontWeight: 600,
                color: "black",
              }}
            >
              <Radio value="M">Male</Radio>
              <Radio value="F">Female</Radio>
            </Radio.Group>
          </Form.Item>
        </Col>
        <Col xs={24} xl={10}>
          <Form.Item
            {...restField}
            name={[name, "dependent_dob"]}
            fieldKey={[fieldKey, "dependent_dob"]}
            initialValue={defaultDOB}
            label="Birth Date"
            rules={[
              {
                required: true,
                message: 'Please select Birth date'
              }
            ]}
          >
            <NcdDatePicker 
              minDate={minDOB} 
              maxDate={maxDOB} 
              isDOB={1}
              minDateMessage={minDOB ? `Birth Date after be after ${minDOB}` : null}
              maxDateMessage={maxDOB ? `Birth Date must be before ${maxDOB}` : null}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24} style={{ float: "right", margin: 5 }}>
        <DeleteOutlined
          onClick={() => {
            remove(name);
            removeEffects();
          }}
          style={{ fontSize: 25 }}
        />
      </Row>
      <br />
    </S.DepBox>
  );
};

export const SpouseInfo = (props) => {
  const {setAddHandler, removeEffects, form} = props;
  const [lastIndex,setLastIndex]=useState(0)
  let maxSpouseDOB = moment().subtract(19, 'years').format('MM/DD/YYYY');
  let minSpouseDOB = '01/01/1920';
  let defaultDOB = moment().subtract(60, 'years').startOf('year').format('MM/DD/YYYY');

  return (
    <div style={{ width: "100%" }}>
      
        <Form.List name="dependent_spouse">
          {(fields, { add, remove }) => (
            <>
              {setAddHandler(add)}
              {fields.map(({ key, name, fieldKey, ...restField }) => {
                setLastIndex(key);
                return (
                  <DepInfoForm 
                    key={key} 
                    name={name} 
                    fieldKey={fieldKey} 
                    restField={restField} 
                    remove={remove}
                    removeEffects={removeEffects}
                    title='Spouse'
                    maxDOB={maxSpouseDOB}
                    minDOB={minSpouseDOB}
                    defaultGender={(form.getFieldValue('applicant_gender') === 'M') ? 'F' : 'M'}
                    defaultDOB={defaultDOB}
                  />
              )})}
            </>
          )}
        </Form.List>
    </div>
  );
};

export const ChildInfo = (props) => {
  const {setAddHandler, removeEffects} = props;
  const [lastIndex,setLastIndex]=useState(0)
  let maxChildDOB = moment().format('MM/DD/YYYY');
  let minChildDOB = moment().subtract(25, 'years').format('MM/DD/YYYY');
  let defaultDOB = moment().subtract(15, 'years').startOf('year').format('MM/DD/YYYY');

  return (
    <div style={{ width: "100%" }}>
      <Form.List name="dependent_children">
        {(fields, { add, remove }) => (
          <>
            {setAddHandler(add)}
              {fields.map(({ key, name, fieldKey, ...restField }) => {
                return(
                  <DepInfoForm
                    key={key} 
                    name={name} 
                    fieldKey={fieldKey} 
                    restField={restField} 
                    remove={remove}
                    removeEffects={removeEffects}
                    title='Child'
                    minDOB={minChildDOB}
                    maxDOB={maxChildDOB}
                    defaultDOB={defaultDOB}
                  />
              )})}
          </>
        )}
      </Form.List>
    </div>
  );
};