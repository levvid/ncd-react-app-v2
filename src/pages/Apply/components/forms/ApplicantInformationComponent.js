/* eslint-disable no-prototype-builtins */
import React, { useState, useEffect, lazy } from "react";
import { Form, Input, Row, Col, Divider, Radio, Checkbox } from "antd";
import MaskedInput from "antd-mask-input";
import moment from "moment";
import axios from "axios";
import configData from "../../../../config";
import NcdDatePicker from "../../../../common/DatePicker";
import { NcdSecondaryButton } from "../../../../common/Button/index";
import AddressBox from "../addressBox";
import ChangeTimeline from "../changeTimeline";
import { useStore } from "../../../../Store";
import { SpouseInfo, ChildInfo } from "../dependentInfo";
import { determineDependentCount, determineDependentInfo } from "../methods";

import * as S from "../../styles";

const SvgIcon = lazy(() => import("../../../../common/SvgIcon"));

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
    xl: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
    xl: {
      span: 24,
    },
  },
};

const ApplicantForm = (props) => {
  const {
    applicantForm,
    setApplicantForm,
    searchFields,
    planForm,
    cartOverview,
    appID,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
    setApplicantForm: state.setApplicantForm,
    searchFields: state.searchFields,
    planForm: state.planForm,
    cartOverview: state.cartOverview,
    appID: state.appID,
  }));

  const [form] = Form.useForm();
  const [formLayout] = useState("vertical"); //setFormLayout

  // const [spouseLeft, setSpouseLeft] = useState(0);
  // const [childLeft, setChildLeft] = useState(0);
  // const [commonMax, setCommonMax] = useState(0);
  const [isChangeTimelineVisible, setIsChangeTimelineVisible] = useState(false);

  // MLG
  const [showAddChild, setShowAddChild] = useState(false); 
  const [showAddSpouse, setShowAddSpouse] = useState(false);
  const [showOnlyOne, setShowOnlyOne] = useState(false);
  // MLG

  useEffect(() => { // MLG
    form.setFieldsValue(applicantForm);
    // form.resetFields();
  }, [applicantForm]);


  useEffect(() => { // MLG
    // let dependentCount = determineDependentCount(planForm, searchFields);
    // setSpouseLeft(dependentCount[0]);
    // setChildLeft(dependentCount[1]);
    // setCommonMax(dependentCount[2]);

    let dependentInfo = determineDependentInfo(planForm, searchFields);
    setShowAddSpouse(dependentInfo[0]);
    setShowAddChild(dependentInfo[1]);
    setShowOnlyOne(dependentInfo[2]);
    console.log("Dependent info: " + JSON.stringify(dependentInfo));
  }, [planForm]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  let childAdder = null;
  let spouseAdder = null;
  let maxApplicantDOB = moment().subtract(19, "years").format("MM/DD/YYYY");
  let minApplicantDOB = "01/01/1920";
  let defaultApplicantDOB = moment()
    .subtract(60, "years")
    .startOf("year")
    .format("MM/DD/YYYY");

  props.setFormHandler(form);

  const handleAfterChangeTimeline = async (success = 1) => {
    if (!success) {
      props.setIsStepSuccessful(false);
      return;
    }

    let values = form.getFieldsValue();

    console.log('Form values: ' + JSON.stringify(values));

    try {
      values.dependent_spouse = values.dependent_spouse || [];
      values.dependent_children = values.dependent_children || [];
      values.email = searchFields.banner_email;
      setApplicantForm(values);
      let res = await axios({
        method: "patch",
        url: `${configData.API_URL}/app`,
        data: {
          app_data: {
            applicantForm: values,
            searchFields: searchFields,
            planForm: planForm,
            cartOverView: cartOverview,
          },
          app_id: appID,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      props.setIsStepSuccessful(true);
    } catch (e) {
      props.setIsStepSuccessful(false);
    }
  };

  const onFinish = async () => {
    if (
      planForm.hasOwnProperty("cov_dates_changed") &&
      planForm.cov_dates_changed
    ) {
      handleAfterChangeTimeline(1);
    } else {
      setIsChangeTimelineVisible(true);
    }
  };

  return (
    <>
      <Form
        {...formItemLayout}
        form={form}
        formLayout={formLayout}
        name="applicant"
        onFinish={onFinish}
        initialValues={{
          state: searchFields.search_state,
          mailing_state: searchFields.search_state,
          email: searchFields.banner_email,
          ...applicantForm,
        }}
        size="large"
        scrollToFirstError={true}
      >
        <Row gutter={36}>
          <Col xl={24} xs={24}>
            <S.headingDiv>
              <S.bluTag />
              <S.headingContent>Contact Information</S.headingContent>
            </S.headingDiv>
          </Col>
          <br />
          <Col xl={24} xs={24}>
            <Row gutter={24} justify="start">
              <Col xs={24} xl={12}>
                <Form.Item
                  name="phone_primary"
                  label="Primary Phone Number"
                  rules={[
                    {
                      required: true,
                      message: "Please enter your phone number",
                    },
                    {
                      pattern: new RegExp(
                        /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/
                      ),
                      message: "Please enter a valid phone no",
                    },
                  ]}
                >
                  <S.NcdMaskedInput
                    style={{
                      width: "100%",
                    }}
                    prefix="+1"
                    mask="111-111-1111"
                  />
                </Form.Item>
              </Col>
              <Col xs={24} xl={12}>
                <Form.Item
                  name="phone_alt"
                  label="Alternate Phone Number"
                  rules={[
                    {
                      pattern: new RegExp(
                        /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/
                      ),
                      message: "Please enter a valid phone no",
                    },
                  ]}
                >
                  <MaskedInput
                    style={{
                      width: "100%",
                    }}
                    prefix="+1"
                    mask="111-111-1111"
                  />
                </Form.Item>
              </Col>
              {/* <Col xs={24} xl={8}>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    // {
                    //   required: true,
                    //   message: "Please enter email",
                    // },
                    {
                      type: "email",
                      message: "Please enter a valid email",
                    },
                  ]}
                >
                  <Input
                  readOnly={true}
                    style={{
                      width: "100%",
                    }}
                    placeholder="EMAIL"
                  />
                </Form.Item>
              </Col> */}
            </Row>
            {/* <Row gutter={24} justify="start">
              <Col xl={24} xs={24}>
                <Form.Item
                  name="tcps"
                  label=""
                  valuePropName="checked"
                  initialValue="checked"
                >
                  <Checkbox
                    style={{
                      margin: "10px 0",
                      display: "flex",
                      justifyContent: "start",
                    }}
                  >
                    <p
                      style={{
                        color: "black",
                        fontSize: 14,
                        display: "flex",
                        justifyContent: "start",
                        textAlign: "left",
                      }}
                    >
                      I consent to receive transactional and promotional
                      communications from NCD.
                    </p>
                  </Checkbox>
                </Form.Item>
              </Col>
            </Row> */}
          </Col>
          <Col xl={24} xs={24}>
            <S.headingDiv>
              <S.bluTag />
              <S.headingContent>
                Primary Application Information
              </S.headingContent>
            </S.headingDiv>
          </Col>
          <Col xl={24} xs={24}>
            <p style={{ color: "black", fontSize: 12, margin: "20px 0px" }}>
              <b>Important Notice:</b> The answers to these questions will be
              electronically transferred to the application for insurance
            </p>
            <p style={{ color: "black", fontSize: "3.45vh" }}>
              <b>Primary Applicant</b>
            </p>
            <Row gutter={24}>
              <Col xl={8} xs={24}>
                <Form.Item
                  name="applicant_fname"
                  label="First Name"
                  rules={[
                    {
                      type: "string",
                      required: true,
                      message: "Please enter First Name",
                    },
                    {
                      pattern: new RegExp(/^[A-Za-z]+$/),
                      message: "Name cannot contain spaces or symbols",
                    },
                  ]}
                >
                  <Input placeholder="first name*" />
                </Form.Item>
              </Col>
              <Col xl={8} xs={24}>
                <Form.Item
                  name="applicant_mname"
                  label="Middle Name (Optional)"
                  rules={[
                    {
                      type: "string",
                    },
                    {
                      pattern: new RegExp(/^[A-Za-z]+$/),
                      message: "Name cannot contain spaces or symbols",
                    },
                  ]}
                >
                  <Input placeholder="Middle name" className="non-mandatory" />
                </Form.Item>
              </Col>
              <Col xl={8} xs={24}>
                <Form.Item
                  name="applicant_lname"
                  label="Last Name"
                  rules={[
                    {
                      type: "string",
                      required: true,
                      message: "Please enter Last Name",
                    },
                    {
                      pattern: new RegExp(/^[A-Za-z]+$/),
                      message: "Name cannot contain spaces or symbols",
                    },
                  ]}
                >
                  <Input placeholder="Last name*" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xl={14} xs={24}>
                <Form.Item
                  name="applicant_dob"
                  label="Birth Date"
                  initialValue={defaultApplicantDOB}
                  rules={[
                    {
                      required: true,
                      message: "Please fill up your Birth date",
                    },
                  ]}
                  disabled={true}
                >
                  <NcdDatePicker
                    maxDate={maxApplicantDOB}
                    minDate={minApplicantDOB}
                    isDOB={1}
                    maxDateMessage={`Birth Date must be before ${maxApplicantDOB}`}
                    minDateMessage={`Birth Date must be after ${minApplicantDOB}`}
                  />
                </Form.Item>
              </Col>
              <Col xl={8} xs={24}>
                <Form.Item
                  label="Gender"
                  name="applicant_gender"
                  wrapperCol={{ sm: 24 }}
                  rules={[
                    {
                      required: true,
                      message: "Please select your gender",
                    },
                  ]}
                >
                  <Radio.Group
                    className="ant-input-affix-wrapper"
                    style={{
                      padding: "8px 5px",
                      fontWeight: 600,
                      color: "black",
                    }}
                  >
                    <Radio value="M">Male</Radio>
                    <Radio value="F">Female</Radio>
                  </Radio.Group>
                </Form.Item>
              </Col>
            </Row>
            <Row> {/*MLG */}
              {showAddSpouse ? (
                <SpouseInfo
                  setAddHandler={(f) => (spouseAdder = f)}
                  removeEffects={() => {
                    if(showOnlyOne) {
                      setShowAddChild(true);
                    }
                    
                  }}
                  form={form}
                />
              ) : null}
            </Row>
            <Row> {/*MLG */}
              {showAddChild ? (
                <ChildInfo
                setAddHandler={(f) => (childAdder = f)}
                removeEffects={() => {
                  if(showOnlyOne && (!form.getFieldValue('dependent_children') || form.getFieldValue('dependent_children').length === 0)) {
                    setShowAddSpouse(true);
                  }
                }}
              />
              ): null}
            </Row>
            <Row gutter={24}> {/*MLG */}
              {showAddSpouse 
              && (!form.getFieldValue('dependent_spouse') || form.getFieldValue('dependent_spouse').length === 0) ? (
                <Col xs={12} xl={6}>
                  <NcdSecondaryButton
                    onClick={() => {
                      spouseAdder();
                      if (showOnlyOne) {
                        setShowAddChild(false);
                      }
                    }}
                  >
                    <SvgIcon src='member_spouse.png' height='24' />&nbsp;Add Spouse
                  </NcdSecondaryButton>
                </Col>
              ) : null}
              {showAddChild ? (
                <Col xs={12} xl={6}> 
                  <NcdSecondaryButton
                    onClick={() => {
                      childAdder();
                      if (showOnlyOne) {
                        setShowAddSpouse(false);
                      }
                    }}
                  >
                    <SvgIcon src='member_children.png' height='24' />&nbsp;Add Child
                  </NcdSecondaryButton>
                </Col>
              ) : null}
            </Row> {/*MLG */}
          </Col>
          <Divider />
          <AddressBox form={form} />
        </Row>
      </Form>
      <ChangeTimeline
        isVisible={isChangeTimelineVisible}
        setIsVisible={setIsChangeTimelineVisible}
        customFunction={handleAfterChangeTimeline}
      />
    </>
  );
};

export default ApplicantForm;
