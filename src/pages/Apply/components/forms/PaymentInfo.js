import React, { useState, lazy } from "react";
import {
  Form,
  Input,
  Row,
  Col,
  Radio,
  Select,
  message,
  Typography,
} from "antd";
import MaskedInput from "antd-mask-input";
import axios from "axios";
import moment from "moment";
import * as S from "../../styles";
import { useStore } from "../../../../Store";
import configData from "../../../../config";
import { PreferredBadge } from "../../../../common/Badge";

const SvgIcon = lazy(() => import("../../../../common/SvgIcon"));

const { Text } = Typography;

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 24,
    },
    xl: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
    xl: {
      span: 24,
    },
  },
};

const ACHPayment = () => {
  const [isMobile] = useState(window.innerWidth <= 768);

  return (
    <S.PaymentBox>
      <h3>Bank Account Information</h3>
      <Row gutter={24}>
        <Col xs={24} xl={8}>
          <Form.Item
            label="Routing Number"
            name="ach_routing_number"
            rules={[
              {
                required: true,
                message: "Please enter routing number",
              },
              {
                pattern: new RegExp(/^[0-9]{0,9}$/),
                message: "Please enter a valid routing number",
              },
            ]}
          >
            <Input placeholder="ENTER NUMBER" />
          </Form.Item>
        </Col>
        <Col xs={24} xl={8}>
          <Form.Item
            label="Account Number"
            name="ach_account_number"
            rules={[
              {
                required: true,
                message: "Please enter account number",
              },
              {
                pattern: new RegExp(/^[0-9]{0,17}$/),
                message: "Please enter a valid account number",
              },
            ]}
          >
            <Input placeholder="ENTER NUMBER" />
          </Form.Item>
        </Col>
        <Col xs={24} xl={8}>
          <Form.Item
            label="Account Type"
            name="ach_account_type"
            rules={[
              {
                required: true,
                message: "Please select account type",
              },
            ]}
          >
            <Select placeholder="SELECT TYPE">
              <Option key={1} value="saving">
                Saving
              </Option>
              <Option key={2} value="checking">
                Checking
              </Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={24} xl={16}>
          <Form.Item
            label="Name of the bank where debit is authorized"
            name="ach_bank_name"
            rules={[
              {
                required: true,
                type: "string",
                message: "Please enter bank name",
              },
            ]}
          >
            <Input placeholder="ENTER NAME" />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xl={1}>
          <SvgIcon src="lock.png" height={20} />
        </Col>
        <Col xl={23}>
          Your Account information is protected using industry standard SSL
          Encryption technology.
        </Col>
      </Row>
      <br />
      <Row gutter={24}>
        <div style={{ width: "100%" }}>
          <b>Example Check</b>
        </div>
        <div>
          <SvgIcon src="sample_cheque.PNG" height={isMobile ? 110 : 200} />
        </div>
      </Row>
    </S.PaymentBox>
  );
};

const CreditCardPayment = (props) => {
  const { form } = props;
  const [creditCardMask, setCreditCardMask] = useState("1111 1111 1111 1111");
  const [ccvMask, setCCVMask] = useState("111");

  const handleCreditCardNumberChange = (e) => {
    let cardNumber = e.target.value;
    if (["34", "37"].includes(cardNumber.substr(0, 2))) {
      setCreditCardMask("1111 111111 11111");
      setCCVMask("1111");
    } else {
      setCreditCardMask("1111 1111 1111 1111");
      setCCVMask("111");
    }
  };

  const expiryYearValidator = (rule, value, callback) => {
    if (value < moment().year() || value > moment().add(25, "years").year()) {
      callback("Please enter a valid year");
    } else {
      callback();
    }
  };

  const creditCardNumberValidator = (rule, value, callback) => {
    let cardNumber = value.replace(/ /g, "");

    if (["34", "36", "37"].includes(cardNumber.substr(0, 2))) {
      if (cardNumber.length !== 15) {
        callback("Please enter a valid card number");
      } else {
        callback();
      }
    } else if (cardNumber.length !== 16) {
      callback("Please enter a valid card number");
    } else {
      callback();
    }
  };

  const ccvValidator = (rule, value, callback) => {
    let cardNumber = form.getFieldValue("card_number") || "";
    cardNumber = cardNumber.replace(/ /g, "");

    if (["34", "37"].includes(cardNumber.substr(0, 2))) {
      if (value.length !== 4) {
        callback("Please enter valid ccv");
      } else {
        callback();
      }
    } else if (value.length !== 3) {
      callback("Please enter valid ccv");
    } else {
      callback();
    }
  };

  return (
    <S.PaymentBox>
      <h3>Credit Card Information</h3>
      <Row gutter={24}>
        <Col xs={24} xl={8}>
          <Form.Item
            label="Card Number"
            name="card_number"
            rules={[
              {
                required: true,
                message: "Please enter card number",
              },
              {
                pattern: new RegExp(/^[0-9 ]+$/),
                message: "Please enter a valid card number",
              },
              {
                validator: creditCardNumberValidator,
              },
            ]}
            validateFirst={true}
          >
            <MaskedInput
              mask={creditCardMask}
              onChange={handleCreditCardNumberChange}
            />
          </Form.Item>
        </Col>
        <Col xs={12} xl={6}>
          <Form.Item
            label="Month"
            name="card_expiry_month"
            rules={[
              {
                required: true,
                message: "Please enter expiry month",
              },
              {
                pattern: new RegExp(/^(0?[1-9]|1[012])$/),
                message: "Please enter a valid month",
              },
            ]}
          >
            <Input placeholder="MM" type="number" />
          </Form.Item>
        </Col>
        <Col xs={12} xl={6}>
          <Form.Item
            label="Year"
            name="card_expiry_year"
            rules={[
              {
                required: true,
                message: "Please enter expiry year",
              },
              {
                pattern: new RegExp(/^[0-9]{4}$/),
                message: "Please enter a valid year",
              },
              {
                validator: expiryYearValidator,
              },
            ]}
            validateFirst={true}
          >
            <Input placeholder="YYYY" type="number" />
          </Form.Item>
        </Col>
        <Col xs={12} xl={4}>
          <Form.Item
            label="CCV"
            name="card_ccv"
            rules={[
              {
                required: true,
                message: "Please enter CCV",
              },
              {
                pattern: new RegExp(/^[0-9]{3,4}$/),
                message: "Please enter a valid CCV",
              },
              {
                validator: ccvValidator,
              },
            ]}
            validateFirst={true}
          >
            <MaskedInput mask={ccvMask} />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={24}>
        <Col xs={1}>
          <SvgIcon src="lock.png" height={20} />
        </Col>
        <Col xs={23}>
          Your Credit Card information is protected using industry standard SSL
          Encryption technology.
        </Col>
      </Row>
      <br />
    </S.PaymentBox>
  );
};

const PaymentForm = (props) => {
  const {
    applicantForm,
    searchFields,
    planForm,
    cartOverview,
    appID,
    prevInsuranceForm,
    paymentInfoForm,
    setPaymentInfoForm,
  } = useStore((state) => ({
    applicantForm: state.applicantForm,
    searchFields: state.searchFields,
    planForm: state.planForm,
    cartOverview: state.cartOverview,
    appID: state.appID,
    prevInsuranceForm: state.prevInsuranceForm,
    paymentInfoForm: state.paymentInfoForm,
    setPaymentInfoForm: state.setPaymentInfoForm,
  }));

  const [form] = Form.useForm();
  const [formLayout, setFormLayout] = useState("vertical");
  const [paymentType, setPaymentType] = useState(
    paymentInfoForm.payment_method_type || "ach"
  );

  props.setFormHandler(form);

  const onFinish = async (values) => {
    try {
      setPaymentInfoForm(values);
      let res = await axios({
        method: "patch",
        url: `${configData.API_URL}/app`,
        data: {
          app_data: {
            searchFields: searchFields,
            planForm: planForm,
            cartOverview: cartOverview,
            applicantForm: applicantForm,
            prevInsuranceForm: prevInsuranceForm,
            paymentInfoForm: {},
            isAgent: true
          },
          app_email: searchFields.banner_email,
          app_id: appID,
        },
        headers: {
          "Content-Type": "application/json",
        },
      });
      props.setIsStepSuccessful(true);
    } catch (e) {
      message.error("Invalid payment info");
      props.setIsStepSuccessful(false);
    }
  };

  return (
    <div style={{ marginBottom: 30 }}>
      <S.headingDiv>
        <S.bluTag />
        <S.headingContent>Payment Methods</S.headingContent>
      </S.headingDiv>
      <br />
      <Form
        {...formItemLayout}
        form={form}
        formLayout={formLayout}
        name="payment_type"
        onFinish={onFinish}
        initialValues={{ payment_method_type: "ach", ...paymentInfoForm }}
        size="large"
        scrollToFirstError
      >
        <p>Please select a payment method</p>
        <Form.Item
          label=""
          name="payment_method_type"
          wrapperCol={{ sm: 24 }}
          rules={[
            {
              required: true,
              message: "Please select products",
            },
          ]}
        >
          <Radio.Group
            style={{ fontWeight: 700, float: "left", padding: 0 }}
            onChange={(e) => {
              setPaymentType(e.target.value);
            }}
          >
            <Radio
              value="ach"
              style={{ verticalAlign: "center", margin: 0, padding: 0 }}
            >
              ACH&nbsp;
              <PreferredBadge />
            </Radio>
            <Radio
              value="credit_card"
              style={{ verticalAlign: "center", margin: 0, padding: 0 }}
            >
              CREDIT CARD
            </Radio>
          </Radio.Group>
        </Form.Item>
        {paymentType === "ach" ? <ACHPayment /> : null}
        {paymentType === "credit_card" ? (
          <CreditCardPayment form={form} />
        ) : null}
      </Form>
    </div>
  );
};

export default PaymentForm;
