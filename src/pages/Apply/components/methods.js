export const determineDependentCount = (planForm, searchFields) => {
  let dental_benefits = planForm.dental_benefits;
  let vision_benefits = planForm.vision_benefits;
  console.log('Vision benefits: ' + vision_benefits + ' dental: ' + dental_benefits);
  /** #### Benefits
  * Member - 51
  * Member + Spouse - 41
  * Member + Children - 677
  * Family - 21
  * ### Visions
* #### Benefits
  * Member - 51
  * Member + 1 - 450
  * Family - 21 */

  if (searchFields.search_product_type == "dental") {
    vision_benefits = -1;
  }

  if (searchFields.search_product_type == "vision") {
    dental_benefits = -1;
  }

  let vision_spouse_count = 0,
    vision_child_count = 0;
  let dental_spouse_count = 0,
    dental_child_count = 0;
  let common_max = -1;

  if (dental_benefits == 21 || dental_benefits == 41) { //family or +spouse
    dental_spouse_count = 1;
  }
  if (dental_benefits == 677 || dental_benefits == 21) { // + children or family
    dental_child_count = 1;
  }

  if (vision_benefits == 21 || vision_benefits == 450) { // family or M+1
    vision_spouse_count = 1;
    vision_child_count = 1;
  }

  let spouse_count = Math.max(dental_spouse_count, vision_spouse_count);
  let child_count = Math.max(dental_child_count, vision_child_count);

  if (vision_benefits == 450) { // M+1
    if (dental_child_count == 1) {
      common_max = -1;
    } else if (dental_benefits == 41) { // M+S
      common_max = 2;
    } else {
      common_max = 1;
    }
  }

  return [spouse_count, child_count, common_max];
};


export const determineDependentInfo = (planForm, searchFields) => { // NEWMLG
  let dental_benefits = planForm?.dental_benefits || 51;
  let vision_benefits = planForm?.vision_benefits || 51;

  let showAddSpouse = false;
  let showAddChild = false;
  let showOnlyOne = false;

  if (searchFields.search_product_type === 'both') {
    if (vision_benefits === 21 || dental_benefits === 21) { // F
      showAddChild = true;
      showAddSpouse = true;
    } else if (vision_benefits === 450) { // M+1
      if (dental_benefits === 41) { // M+S
        showAddSpouse = true;
        showAddChild = false;
      } else if (dental_benefits === 677) { // M+C
        showAddSpouse = false;
        showAddChild = true;
      } else { // M
        showAddChild = true;
        showAddSpouse = true;
        showOnlyOne = true;
      }
    } else if (dental_benefits === 41 && vision_benefits === 51) { // M+S
      showAddSpouse = true;
      showAddChild = false;
    } else if (dental_benefits === 677 && vision_benefits === 51) { // M+C
      showAddSpouse = false;
      showAddChild = true;
    } else { // D - M, V - M
      showAddChild = false;
      showAddSpouse = false;
    }
  } else if (searchFields.search_product_type === 'vision') {
    if (vision_benefits === 51) {
      showAddChild = false;
      showAddSpouse = false;
    } else if (vision_benefits === 450) {
      showAddChild = true;
      showAddSpouse = true;
      showOnlyOne = true;
    } else if (vision_benefits === 21) {
      showAddChild = true;
      showAddSpouse = true;
    } 
  } else if (searchFields.search_product_type === 'dental') {
    if (dental_benefits === 51) {
      showAddChild = false;
      showAddSpouse = false;
    } else if (dental_benefits == 41) {
      showAddChild = false;
      showAddSpouse = true;
    } else if (dental_benefits === 677) {
      showAddChild = true;
      showAddSpouse = false;
    } else if (dental_benefits === 21) {
      showAddChild = true;
      showAddSpouse = true;
    }
  } 

  return [showAddSpouse, showAddChild, showOnlyOne];

};