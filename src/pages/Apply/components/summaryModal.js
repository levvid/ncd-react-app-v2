import { lazy, useState } from "react";
import { Divider, Button, Modal, Row, Col } from "antd";
import * as S from "../styles";
import { useStore } from "../../../Store";

export default function SummaryModal({ isVisible, setIsVisible }) {
  const { applicantForm, paymentForm, appID, prevInsurance, planForm } =
    useStore((state) => ({
      applicantForm: state.applicantForm,
      appID: state.appID,
      paymentForm: state.paymentInfoForm,
      prevInsurance: state.prevInsuranceForm,
      planForm: state.planForm,
    }));

  
  const handleOk = async () => {
    setIsVisible(false);
  };

  const handleCancel = async () => {
    setIsVisible(false);
  };

  let dependent_children_key = 0;

  return (
    <>
      <Modal
        title="Review Application"
        visible={isVisible}
        centered
        width={1200}
        style={{ padding: "10px", fontSize: 5, top: 0}}
        footer={[
          <Button key="cancel" onClick={handleCancel} type="primary">
            Close
          </Button>,
        ]}
        onCancel={() => setIsVisible(false)}
      >
        <Row style={{overflowY: 'scroll', height: '50vh'}}>
          <Col xs={24} xl={24}>
            <Row>
              <Col xs={24} xl={24}>
                <S.headingDiv>
                  <S.headingContent>Personal Information</S.headingContent>
                </S.headingDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>Name</S.modalfieldName>
                  <S.modalfieldName>{`${applicantForm.applicant_fname} ${
                    applicantForm.applicant_mname || ""
                  } ${applicantForm.applicant_lname}`}</S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>Gender</S.modalfieldName>
                  <S.modalfieldName>
                    {applicantForm.applicant_gender == "M" ? "Male" : "Female"}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>
                    Birth Date
                  </S.modalfieldName>
                  <S.modalfieldName>
                    {applicantForm.applicant_dob}
                  </S.modalfieldName>
                </S.modalRowDiv>
                {(applicantForm?.dependent_spouse?.length || 0) > 0 ? ( // MLG
                  <>
                    <S.headingDiv>
                      <S.headingContent>Spouse Information</S.headingContent>
                    </S.headingDiv>
                    <S.modalRowDiv>
                      <S.modalfieldName fontWeight={600}>Name</S.modalfieldName>
                      <S.modalfieldName>{`${
                        applicantForm.dependent_spouse[0].dependent_fname
                      } ${
                        applicantForm.dependent_spouse[0].dependent_mname || ""
                      } ${
                        applicantForm.dependent_spouse[0].dependent_lname
                      }`}</S.modalfieldName>
                    </S.modalRowDiv>
                    <S.modalRowDiv>
                      <S.modalfieldName fontWeight={600}>
                        Gender
                      </S.modalfieldName>
                      <S.modalfieldName>
                        {applicantForm.dependent_spouse[0].dependent_gender ==
                        "M"
                          ? "Male"
                          : "Female"}
                      </S.modalfieldName>
                    </S.modalRowDiv>
                    <S.modalRowDiv>
                      <S.modalfieldName fontWeight={600}>
                        Birth Date
                      </S.modalfieldName>
                      <S.modalfieldName>
                        {applicantForm.dependent_spouse[0].dependent_dob}
                      </S.modalfieldName>
                    </S.modalRowDiv>
                  </>
                ) : null}
                {(applicantForm?.dependent_children?.length || 0) > 0 ? ( // MLG
                  <>
                    <S.headingDiv>
                      <S.headingContent>
                        {(applicantForm.dependent_children.length || 0) == 1
                          ? "Child"
                          : "Children"}{" "}
                        Information
                      </S.headingContent>
                    </S.headingDiv>
                    {applicantForm.dependent_children.map((child, index) => {
                      return (
                        <Row
                          style={{ marginBottom: "10px" }}
                          key={dependent_children_key++}
                        >
                          <Col xs={2} xl={2}>
                            <S.modalfieldName>{index + 1}.</S.modalfieldName>
                          </Col>
                          <Col xs={22} xl={22}>
                            <S.modalRowDiv>
                              <S.modalfieldName fontWeight={600}>
                                Name
                              </S.modalfieldName>
                              <S.modalfieldName>{`${child.dependent_fname} ${
                                child.dependent_mname || ""
                              } ${child.dependent_lname}`}</S.modalfieldName>
                            </S.modalRowDiv>
                            <S.modalRowDiv>
                              <S.modalfieldName fontWeight={600}>
                                Gender
                              </S.modalfieldName>
                              <S.modalfieldName>
                                {child.dependent_gender == "M"
                                  ? "Male"
                                  : "Female"}
                              </S.modalfieldName>
                            </S.modalRowDiv>
                            <S.modalRowDiv>
                              <S.modalfieldName fontWeight={600}>
                                Birth Date
                              </S.modalfieldName>
                              <S.modalfieldName>
                                {
                                  applicantForm.dependent_children[0]
                                    .dependent_dob
                                }
                              </S.modalfieldName>
                            </S.modalRowDiv>
                          </Col>
                        </Row>
                      );
                    })}
                  </>
                ) : null}
              </Col>
              <Col xs={0} sm={1}>
                <Divider type="vertical" style={{ height: "100%" }}></Divider>
              </Col>
              <Col xs={24} xl={24}>
                <S.headingDiv>
                  <S.headingContent>Address</S.headingContent>
                </S.headingDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}> Street </S.modalfieldName>
                  <S.modalfieldName
                    style={{ width: "80%", textAlign: "right" }}
                  >
                    {applicantForm.address1}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>
                    Apartment
                  </S.modalfieldName>
                  <S.modalfieldName
                    style={{ width: "80%", textAlign: "right" }}
                  >
                    {applicantForm.address2}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}> City </S.modalfieldName>
                  <S.modalfieldName
                    style={{ width: "80%", textAlign: "right" }}
                  >
                    {applicantForm.city}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}> State </S.modalfieldName>
                  <S.modalfieldName
                    style={{ width: "80%", textAlign: "right" }}
                  >
                    {applicantForm.state}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>
                    Postal Code
                  </S.modalfieldName>
                  <S.modalfieldName
                    style={{ width: "80%", textAlign: "right" }}
                  >
                    {applicantForm.zip_code}
                  </S.modalfieldName>
                </S.modalRowDiv>
                <S.headingDiv>
                  <S.headingContent>Contact Details</S.headingContent>
                </S.headingDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>Email</S.modalfieldName>
                  <S.modalfieldName>{applicantForm.email}</S.modalfieldName>
                </S.modalRowDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>
                    Phone No.
                  </S.modalfieldName>
                  <S.modalfieldName>{applicantForm.phone_primary}</S.modalfieldName>
                </S.modalRowDiv>
                <S.headingDiv>
                  <S.headingContent>Payment Information</S.headingContent>
                </S.headingDiv>
                <S.modalRowDiv>
                  <S.modalfieldName fontWeight={600}>
                    {" "}
                    Payment Type
                  </S.modalfieldName>
                  <S.modalfieldName>
                    {paymentForm.payment_method_type == "credit_card"
                      ? "Credit Card"
                      : "Monthly Bank Draft(ACH)"}
                  </S.modalfieldName>
                </S.modalRowDiv>

                {["dental", "both"].includes(planForm.search_product_type) ? (
                  <>
                    <S.headingDiv>
                      <S.headingContent>
                        Previous Insurance Details
                      </S.headingContent>
                    </S.headingDiv>
                    <S.modalRowDiv>
                      <S.modalfieldName fontWeight={600}>
                        Previous Insurance Type
                      </S.modalfieldName>
                      <S.modalfieldName style={{ textAlign: "right" }}>
                        {prevInsurance.prev_insurance_type.toUpperCase()}
                      </S.modalfieldName>
                    </S.modalRowDiv>
                  </>
                ) : null}
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal>
    </>
  );
}
