const routes = [
  {
    path: ["/", "/home"],
    exact: true,
    component: "Home",
  },
  {
    path: ["/apply"],
    exact: true,
    component: "Apply",
  },
  {
    path: ["/agent"],
    exact: true,
    component: "Agent",
  },
];

export default routes;
